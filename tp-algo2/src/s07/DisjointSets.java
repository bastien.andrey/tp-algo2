package s07;

import java.util.LinkedList;
import java.util.Queue;
// ------------------------------------------------------------
public class DisjointSets {
  private final int[] set;
  private final int[] minimums;
  private int numOfRoots;
  private final int ROOT_INDEX = -1;

  // PRE: nbOfElements >= 0
  public DisjointSets(int nbOfElements) {
    set = new int[nbOfElements];
    minimums = new int[nbOfElements];
    numOfRoots = nbOfElements;
    // Set every element as root
    for (int i = 0; i < set.length; i++) {
      set[i] = ROOT_INDEX;
      minimums[i] = i;
    }
  }

  /** @return root index of set where i is stored
   * PRE 0 <= i < nbOfElements() */
  private int root(int i) {
    if (set[i] <= ROOT_INDEX) return i;
    int root = root(set[i]);
    set[i] = root;
    return root;
  }

  /** determines whether i and j are in the same group.
   *  PRE: 0 <= i,j < nbOfElements() */
  public boolean isInSame(int i, int j) {
    return root(i) == root(j);
  }

  /** merges the respective groups containing i and j 
  *   PRE: 0 <= i,j < nbOfElements() */
  public void union(int i, int j) {
    // Don't merge element that are already in the same set
    if (isInSame(i, j)) return;
    numOfRoots--;
    int rootI = root(i);
    int rootJ = root(j);
    int minimum = Math.min(minimums[rootI], minimums[rootJ]);
    if (set[rootI] <= set[rootJ]) {
      set[rootI] += set[rootJ];
      set[rootJ] = i;
      minimums[rootI] = minimum;
    }
    else {
      set[rootJ] += set[rootI];
      set[rootI] = j;
      minimums[rootJ] = minimum;
    }
  }

  public int nbOfElements() {  // as given in the constructor
    return set.length;
  }
  
  /** @return the smallest value in the same group as i 
   *  PRE: 0 <= i < nbOfElements() */
  public int minInSame(int i) {
    return minimums[root(i)];
  }

  /** @return true if all elements are in the same group */
  public boolean isUnique() {
    return numOfRoots == 1;
  }

  /** String format like this: "{0}{3,2,4}{1,5}" */ 
  @Override public String toString() {
    String res = "";
    int n = nbOfElements();
    for(int i=0; i<n; i++) {
      if (set[i] > ROOT_INDEX) continue;
      res += "{" + i;
      Queue<Integer> q = new LinkedList<>();  // for breadth-first traversal
      q.add(i);
      while(!q.isEmpty()) {
        int v = q.remove();
        for(int j=0; j<n; j++) {
          if (isInSame(j, v) && i != j) {
            res += "," + j;
          }
        }
      }
      res += "}";
    }
    return res;
  }

  /** whether both DisjointSets represent the same logical set of groups */
  @Override public boolean equals(Object otherDisjSets) {
    if (otherDisjSets instanceof DisjointSets otherSet) {
      if (otherSet.nbOfElements() != nbOfElements()) return false;
      for (int i = 0; i < nbOfElements(); i++) {
        if (otherSet.minInSame(i) != minInSame(i))
          return false;
      }
      return true;
    }
    else {
      return false;
    }
  }

  public static void main(String[] args) {
    DisjointSets set = new DisjointSets(10);
    set.union(1, 7);
    set.union(1, 2);
    set.union(6, 9);
    System.out.println(set);
  }
}
