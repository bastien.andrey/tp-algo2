package s10;

import java.util.HashSet;
import java.util.Random;

public class TreapTestNRots {
    public static void main(String[] args) {
        Random r = new Random();
        int[] ns = new int[] {10,50,100,500,1000,5000,10000,1000000};
        for (int n: ns) {
            Treap<Integer> a = new Treap<>();
            HashSet<Integer> b = new HashSet<>();
            System.out.println("n: " + n);
            for (int i = 0; i < n; i++) {
                int pulled = r.nextInt();
                a.add(pulled);
                b.add(pulled);
            }
            System.out.println("Rotations for add: " + a.nRotsAdd);
            for (Integer elt: b) {
                a.remove(elt);
            }
            System.out.println("Rotations for add: " + a.nRotsDel);
        }
    }
}
