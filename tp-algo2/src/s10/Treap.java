package s10;
import java.util.Random;
//--------------------------------------------------
public class Treap<E extends Comparable<E>> {
  public int nRotsAdd =0; // represents of the number of rotations during the life of the object to add elements
  public int nRotsDel =0; // represents of the number of rotations during the life of the object to delete elements
  //============================================================
  static class TreapElt<E extends Comparable<E>> implements Comparable<TreapElt<E>> {
    static final Random rnd = new Random();
    // -----------------------
    private final E elt;
    private int     pty;
    // -----------------------
    public TreapElt(E e) {
      elt=e; 
      pty=rnd.nextInt();
    }

    public int pty() {
      return pty;
    }

    public E elt() {
      return elt;
    }

    public int compareTo(TreapElt<E> o) {
      return elt.compareTo(o.elt);
    }

    @Override public boolean equals(Object o) {
      if(o==null) return false;
      if (this.getClass() != o.getClass()) return false;
      if (elt==null) return false;
      return elt.equals(((TreapElt<?>)o).elt);
    }

    @Override public String toString() {
      return "" + elt + "#" + pty;
    }

    @Override public int hashCode() {
      return elt.hashCode();
    }
  }
  //============================================================
  private final BST<TreapElt<E>> bst;
  // --------------------------
  public Treap() {
    bst=new BST<>();
  }

  /**
   * @author NoeJuza
   * @param e the element you want to add to the structure
   * Adds an element to the structure
   */
  public void add(E e) {
    if (contains(e)) return;
    TreapElt<E> elt = new TreapElt<>(e);
    bst.add(elt);
    BTreeItr<TreapElt<E>> ti = bst.locate(elt);
    percolateUp(ti);
  }

  /**
   * @author NoeJuza
   * @param e the element you want to remove
   * Removes an element from the structure
   */
  public void remove(E e) {
    if (!contains(e)) return;
    siftDownAndCut(bst.locate(new TreapElt<>(e)));
  }

  public boolean contains(E e) {
    return !bst.locate(new TreapElt<>(e)).isBottom();
  }

  public int size() {
    return bst.size();
  }

  public E minElt() {
    return bst.maxElt().elt();
  }

  public E maxElt() {
    return bst.minElt().elt();
  }
  
  public String toString() {
    return bst.toString();
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------

  /**
   * @author noejuza
   * Sifts the current iterator down until it is a leaf and then removes it from the structure
   * @param ti an iterator over the element you want to sift down
   */
  private void siftDownAndCut(BTreeItr<TreapElt<E>> ti) {
    if (ti.isBottom()) {
      System.out.println("was bottom");
      return;
    }
    while (hasLeftChild(ti) || hasRightChild(ti)){
      if (hasRightChild(ti) && hasLeftChild(ti)){
        if (isLess(ti.right(),ti.left())){
          ti.rotateLeft();
          ti = ti.left();
          nRotsDel++;
        }else {
          ti.rotateRight();
          ti = ti.right();
          nRotsDel++;
        }
      }else if( hasLeftChild(ti)) {
        ti.rotateRight();
        ti = ti.right();
        nRotsDel++;
      }else { // only has right child (weird asf)
        ti.rotateLeft();
        ti = ti.left();
        nRotsDel++;
      }
    }
    bst.remove(ti.consult());
  }

  /**
   * @author NoeJuza
   * @param ti iterator you want to know if it has a right child
   * @return true if it has a right child
   */
  private boolean hasRightChild(BTreeItr<TreapElt<E>> ti){
    return !ti.right().isBottom();
  }
  /**
   * @author NoeJuza
   * @param ti iterator you want to know if it has a left child
   * @return true if it has a left child
   */
  private boolean hasLeftChild(BTreeItr<TreapElt<E>> ti){
    return !ti.left().isBottom();
  }

    private void percolateUp(BTreeItr<TreapElt<E>> ti) {
    while((!ti.isRoot()) && isLess(ti, ti.up())) {
      if (ti.isLeftArc()) {ti=ti.up(); ti.rotateRight(); nRotsAdd++;}
      else                {ti=ti.up(); ti.rotateLeft(); nRotsAdd++;}
    }
  }

  private boolean isLess(BTreeItr<TreapElt<E>> a, BTreeItr<TreapElt<E>> b) {
    TreapElt<E> ca = a.consult();
    TreapElt<E> cb = b.consult();
    return ca.pty() < cb.pty();
  }
}
