
package s13;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.function.*;
import java.util.stream.*;

public class S13 {
  
  // ------ Ex. 1 -----------
  public static void printSomePowersOfTwo() {
    // TODO ...
  }

    // ------ Ex. 2 -----------
  public static String macAddress(int[] t) {
    return Arrays.stream(t)
            .mapToObj(x -> String.format("%02X",x))
            .collect(Collectors.joining(":"));
  }
  
  // ------ Ex. 3 -----------

  public static long[] divisors(long n) {
    return LongStream.range(1, n)
            .filter(x -> n % x == 0)
            .toArray();
  }

  // ------ Ex. 4a -----------
  public static long sumOfDivisors(long n) {
    return Arrays.stream(divisors(n)).sum();
  }
  
  
  // ------ Ex. 4b -----------
  public static long[] perfectNumbers(long max) {
    return LongStream.range(0,max)
            .filter(x -> Arrays.stream(divisors(x)).sum() == x)
            .toArray();
  }
  
  // ------ Ex. 5 -----------
  public static void printMagicWord() throws IOException {
    Path path = FileSystems.getDefault().getPath("wordlist.txt");
    Files.lines(path)
      .filter(x -> x.length() == 11)
      .filter(x -> x.charAt(2) == 't')
      .filter(x -> x.charAt(4) == 'l')
      .filter(x -> x.chars().distinct().count() == 6)
      .forEach(System.out::println);
   }
  
  public static boolean isPalindrome(String str) {
    return str.equals(new StringBuilder(str).reverse().toString());
  }

  public static void printPalindromes() throws IOException {
    // TODO ...
  }

  // ------ Ex. 6 -----------
  public static void averages() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };

    double[] grades = new double[results.length];
    for (int i = 0; i < results.length; i++) {
        double sum = 0;
        for (int j = 0; j < results[i].length; j++) {
            sum += results[i][j];
        }
        grades[i] = 1.0 + sum / 10.0;
    }
    double sum = 0;
    for (int i = 0; i < grades.length; i++) {
        sum += grades[i];
    }
    double average = sum / grades.length;

    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  public static void averagesWithStreams() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };
    double[] grades = Arrays.stream(results)
            .mapToDouble(x -> ((Arrays.stream(x).sum())/10)+1)
            .toArray();
    double average = Arrays.stream(grades)
            .sum() / grades.length;
    
    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  // ------ Ex. 8 -----------

  static DoubleStream sampling(double from, double to, int nSubSamples) {
    ArrayList<Double> datas = new ArrayList<>();
    double step = (to - from)/nSubSamples;
    datas.add(from);
    for (int i = 1; i < nSubSamples; i++) {
      datas.add(datas.get(i-1) + step);
    }
    return datas.stream().mapToDouble(x -> x);
  }
  
  public static void ex8() {
    // we could merge it all as one liners, but let's focus on readability
    // System.out.println(... product of:  1.2, 3.4, 5.6
    Arrays.stream(new double[]{1.2, 3.4, 5.6})
            .reduce((a,b) -> a*b)
            .ifPresent(System.out::println);
    // System.out.println(... math series:  i/2^i for i between 4 and 30
    System.out.println(
            IntStream.range(4, 30)
            .mapToDouble(i -> (i / Math.pow(2, i)))
            .sum()
    );
    // System.out.println(... concatenation of: {"Hello","World","!"}
    Arrays.stream(new String[] {"Hello", "Wold", "!"})
            .reduce((a,b) -> a + b)
            .ifPresent(System.out::println);
    // System.out.println(... max of: sin^2*cos in [pi/4..2pi/3], 2002 samples
   System.out.println(Arrays.toString(
                   sampling(Math.PI / 4, (2 * Math.PI) / 3, 2002)
                   .map(x -> Math.pow(Math.sin(x), 2) * Math.cos(x))
                   .toArray())
   );
  }

  // ------ Ex. 9 -----------
  public static void nipas(int n) {
    System.out.println(
      IntStream.range(0, n)
      .mapToObj(
        i -> IntStream.range(i, i+4)
             .mapToObj(j ->
                 " ".repeat(n+2-j) +
                 "*".repeat(1+2*j) )
             .collect(Collectors.joining("\n")))
      .collect(Collectors.joining("\n"))
    );
  }

  //-------------------------------------------------------
  public static void main(String[] args) {
    //printSomePowersOfTwo();
    System.out.println("------ Exercice 2 ------");
    System.out.println(macAddress(new int[]{78, 26, 253, 6, 240, 13}));
    System.out.println("------ Exercice 3 ------");
    System.out.println(Arrays.toString(divisors(496L)));
    System.out.println("------ Exercice 4 ------");
    System.out.println(sumOfDivisors(496L));

    System.out.println(Arrays.toString(perfectNumbers(10_000)));
    /*
    try {
      printMagicWord();
      printPalindromes();
    } catch(IOException e) {
      System.out.println("!! Problem when reading file... " + e.getMessage());
    }
    */
    System.out.println("------ Exercice 6 ------");
    averages();
    averagesWithStreams();

    System.out.println("------ Exercice 8 ------");
    ex8();
    /*
    // nipas(4); // read/analyze the code first!...
    */
  }
  static class DoubleStepSupplier implements DoubleSupplier{
    private final double from;
      private final double step;
      private double last = 0;

    /**
     * DoubleStepSupplier is a DoubleSupplier that returns numbers separated by a "delta" that is calculated with
     * the starting point (from), an arbitrary point (to) and a number of "even cuts" in the line between from and to
     * @author NoeJuza
     * @param from the first element you want to be supplied
     * @param to arbitrary number, must be greater than from
     * @param num the number of "even cuts" you want between the two numbers as a delta
     */
    DoubleStepSupplier(double from, double to, int num){
      this.from = from;
      this.step = (to - from) /num;
    }
    @Override
    public double getAsDouble() {
      if (0 == last){
        last = from;
      }else {
        last += step;
      }
      return last;
    }
  }
}
