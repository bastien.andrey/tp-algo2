package s19;

import java.util.List;
import java.util.ArrayList;

public class BipartiteAnalysis {
  private final UGraph    graph;
  private boolean         isBipartite;
  private final boolean[] isVisited;
  private final boolean[] isWhite;  // meaningful if isBipartite
  private final int[]     parent;   // the traversal tree structure
  private List<Integer>   oddCycle; // meaningful if !isBipartite

  public BipartiteAnalysis(UGraph g) {
    int n = g.nbOfVertices();
    graph = g;
    isVisited = new boolean[n];
    isWhite = new boolean[n];
    parent = new int[n];

    isBipartite = true;
    for (int i = 0; i < n; i++) {
      if (!isVisited[i]) {
        dft(i);
        if (!isBipartite) break;
      }
    }
  }

  // Depth-first traversal
  // PRE: vid is not visited, but has been assigned a color
  // POST: either the whole subtree has been 2-colored, 
  //           or an odd cycle has been found via a back edge
  //              (meaning the graph is not bipartite).
  // Caution: exit the traversal once you find an "odd-cycle";
  //          a "back edge" is later met as a "forward" one...
  private void dft(int vid) {
    isVisited[vid] = true;
    for (int neighbour : graph.neighboursOf(vid)) {
      if (isVisited[neighbour]) {
        if (isWhite[vid] == isWhite[neighbour]) {
          isBipartite = false;
          oddCycle = new ArrayList<>();
          rememberOddCycle(neighbour, vid);
        }
      }
      else {
        parent[neighbour] = vid;
        isWhite[neighbour] = !isWhite[vid];
        dft(neighbour);
      }
      if (!isBipartite) break;
    }
  }

  // build the oddCycle list with the tree path:
  //            <descendantId, parent, ..., ancestorId>
  private void rememberOddCycle(int ancestorId, int descendantId) {
    oddCycle.add(descendantId);
    if (descendantId != ancestorId)
      rememberOddCycle(ancestorId, parent[descendantId]);
  }

  public boolean isBipartite() {
    return isBipartite;
  }

  // PRE: isBipartite()
  // Returns 0 or 1, so that it makes a 2-coloring of the graph
  public int colorOf(int vid) {
    return isWhite[vid] ? 0 : 1;
  }

  // PRE: !isBipartite()
  // Returns an odd-size cycle, eg [a,b,c] for a->b->c->a->b->…
  public List<Integer> anOddCycle() {
    return new ArrayList<>(oddCycle);
  }
}
