package s08;

public class SplayTree<K extends Comparable<K>> {
  BTree<K> tree = new BTree<>();
  int crtSize = 0;

  public SplayTree() { super(); }

  /***
   * @author NoeJuza
   * @param e the ellement you want to add
   * Adds an ellement to the current splay tree as per the scematics
   */
  public void add(K e) {
    if (contains(e)) return;  // This "splays" the tree!
    crtSize++;
    BTreeItr<K> itr = tree.root();
    if (itr.isBottom()) {
      itr.insert(e);
    } else if (e.compareTo(itr.consult()) > 0) {
      BTree<K> a = itr.right().cut();
      BTree<K> b = itr.cut();
      itr.insert(e);
      itr.right().paste(a);
      itr.left().paste(b);
    } else {
      BTree<K> a = itr.left().cut();
      BTree<K> b = itr.cut();
      itr.insert(e);
      itr.left().paste(a);
      itr.right().paste(b);
    }
  }

  public void remove(K e) {
    if (! contains(e)) return; // This "splays" the tree!
    crtSize--;
    if (tree.root().hasLeft() && tree.root().hasRight()) {
      BTree<K> oldRight = tree.root().right().cut();
      tree=tree.root().left().cut();
      BTreeItr<K> maxInLeft = tree.root().rightMost().up();
      BTreeItr<K> ti = splayToRoot(maxInLeft); // now tree has no right subtree!
      ti.right().paste(oldRight);
    } else {  // the tree has only one child
      if (tree.root().hasLeft()) tree = tree.root().left() .cut();
      else                       tree = tree.root().right().cut();
    }
  }

  public boolean contains(K e) {
    if (isEmpty()) return false;
    BTreeItr<K> ti = locate(e);
    boolean absent = ti.isBottom();
    if (absent) ti = ti.up();
    ti = splayToRoot(ti);
    return !absent;
  }

  protected  BTreeItr<K> locate(K e) {
    BTreeItr<K> ti = tree.root();
    while(!ti.isBottom()) {
      K c = ti.consult();
      if (e.compareTo(c) == 0) break;
      if (e.compareTo(c) <  0) ti = ti.left();
      else                     ti = ti.right();
    }
    return ti;
  }

  public int size() { return crtSize; }

  public boolean isEmpty() { return size() == 0; }

  /***
   * @author NoeJuza
   * @return the min element (left-most) of the current splay tree
   */
  public K minElt() {
    if (isEmpty()) return null;
    BTreeItr<K> itr = new BTreeItr<>(tree);
    return itr.leftMost().up().consult();
  }
  /**
   * @author Noejuza
   * @return the max element (right-most) of the current splay tree
   */
  public K maxElt() {
    if (isEmpty()) return null;
    BTreeItr<K> itr = new BTreeItr<>(tree);
    return itr.leftMost().up().consult();
  }

  @Override public String toString() {
    return "" + tree.toReadableString() + "SIZE:" + size();
  }
  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  // PRE:     ! ti.isBottom()
  // RETURNS: root position
  // WARNING: ti is no more valid afterwards
  /***
   * @author NoeJuza
   * @param ti the iterator we want to splay to root
   * @return an iterator over the root of the tree after the splaytoroot operation
   * PRE: !ti.isBottom() WARNING: ti is no more valid afterwards
   */
  private BTreeItr<K> splayToRoot(BTreeItr<K> ti) {
    assert (!ti.isBottom());
    while (!ti.isRoot()) {
      if (shouldZigZag(ti)) ti = applyZigZag(ti);
      else if (shouldZigZig(ti)) ti = applyZigZig(ti);
      else ti = applyZig(ti);
    }
    return ti;
  }
  /***
   * @author NoeJuza
   * @param ti iterator over the element we want to know if we should zigzag
   * @return true if it should, else false
   * helper method to make splayToRoot more readable
   */
  private boolean shouldZigZag(BTreeItr<K> ti){
    return (!ti.up().isRoot() && (
            (ti.isLeftArc() && !ti.up().isLeftArc())
                    ||
                    (!ti.isLeftArc() && ti.up().isLeftArc())
    )
    );
  }
  /***
   * @author NoeJuza
   * @param ti iterator over the element we want to know if we should zigzig
   * @return true if it should, else false
   * helper method to make splayToRoot more readable
   */
  private boolean shouldZigZig(BTreeItr<K> ti){
    return (!ti.up().isRoot() && (
            (ti.isLeftArc() && ti.up().isLeftArc())
                    ||
                    (!ti.isLeftArc() && !ti.up().isLeftArc())
    )
    );
  }

  // PRE / RETURNS : Zig situation (see schemas)
  // WARNING: ti is no more valid afterwards
  private BTreeItr<K> applyZig(BTreeItr<K> ti) {
    boolean leftZig = ti.isLeftArc();
    ti = ti.up();
    if (leftZig) ti.rotateRight();
    else         ti.rotateLeft();
    return ti;
  }

  /**
   * @author NoeJuza
   * @param ti the iterator over the node we want to be on top at the end (c.f schematics)
   * @return the iterator over the node after the operation.
   * This method applies the ZigZig (either left or right) operation using only "rotate" operations.
   * WARNING: ti is invalid after the operation because we mutated the structure
   * PRE: has a grandparent and respects the "base" schematics of zigzig (either left or right)
   */
  private BTreeItr<K> applyZigZig(BTreeItr<K> ti) {
    boolean isLeft  = ti.isLeftArc();
    ti = ti.up();
    if (isLeft){
      ti.rotateRight();
      ti = ti.up();
      ti.rotateRight();
      ti = ti.right();
      ti.rotateRight();
      ti = ti.up();
    }else {
      ti.rotateLeft();
      ti = ti.up();
      ti.rotateLeft();
      ti = ti.left();
      ti.rotateLeft();
      ti = ti.up();
    }
    return ti;
  }
  /**
   * @author NoeJuza
   * @param ti the iterator over the node at which we want to perform the zigzag (c.f. schematics)
   * @return an iterator over the node after the operation, (c.f. schematics)
   * WARNING: ti is not valid anymore because we have mutated the structure
   * PRE: ti should respect the zigzag operation (either left or right) as per schematics
   * This method applies the ZigZag operation using only applyZig() calls.
   */
  private BTreeItr<K> applyZigZag(BTreeItr<K> ti) {
    ti = applyZig(ti);
    ti = applyZig(ti);
    return ti;
  }
}
