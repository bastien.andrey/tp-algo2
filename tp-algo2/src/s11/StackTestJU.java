package s11;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

public class StackTestJU {
  private IntStack s1, s2, s3;

  @Before
  public void setUp() {
    s1 = new IntStack(10);
    s2 = new IntStack();
    s3 = new IntStack(1);
  }

  @Test
  public void testNewIsEmpty() {
    assertTrue(s1.isEmpty());
    assertTrue(s2.isEmpty());
  }

  @Test
  public void testPushThenPop() {
    s1.push(4);
    assertEquals(4, s1.pop());
  }

  @Test
  public void testPushAndPopAndTop() {
    for (int i = 0; i <= 100000; i++) {
      s1.push(i);
      assertEquals(i, s1.top());
      assertFalse(s1.isEmpty());
    }
    for (int i = 100000; i >= 0; i--) {
      assertEquals(i, s1.pop());
    }
    assertTrue(s1.isEmpty());
  }

  @Test
  public void testPushAndPopOnParallelStacks() {
    for (int i = 0; i < 1000; i++) {
      s1.push(i);
      s2.push(i+1);
      assertFalse(s1.isEmpty());
      assertFalse(s2.isEmpty());
      assertEquals(i, s1.pop());
      assertEquals(i+1, s2.pop());
    }
    assertTrue(s1.isEmpty());
    assertTrue(s2.isEmpty());
  }

  @Test
  public void testCheckSize() {
    assertTrue(s3.isEmpty());
    s3.push(1);
    s3.push(2);
    s3.pop();
    assertEquals(1, s3.pop());
  }
}
