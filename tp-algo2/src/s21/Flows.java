package s21;
import java.util.*;

public class Flows {
  //==================================================
  public record MaxFlowResult(
      WeightedDiGraph flow,
      long      throughput,
      boolean[] isInSourceGroup // gives the Min-Cut solution
  ) {}
  //==================================================
  public static MaxFlowResult maxFlow(WeightedDiGraph capacity, 
                                      int source,  int sink) {
    WeightedDiGraph resid = initialResid(capacity);
    WeightedDiGraph flow  = initialFlow (capacity);
    List<Integer> path;

    while (true) {
      path = resid.pathBetween(source, sink);
      if (path == null) break;
      int minStep = minStep(resid, path);
      updateFlow(flow, resid, path, minStep);
    }

    suppressEmptyEdges(flow);

    long throughput = 0;
    for (int edge : flow.neighboursFrom(sink)) {
      throughput += flow.edgeWeight(edge, sink);
    }

    int size = resid.nbOfVertices();
    boolean[] isInSourceGroup = new boolean[size];
    for (int i = 0; i < size; i++) {
      isInSourceGroup[i] = resid.pathBetween(source, i) != null;
    }

    return new MaxFlowResult(flow, throughput, isInSourceGroup);
  }

  public static void suppressEmptyEdges(WeightedDiGraph g) {
    for(WeightedDiGraph.Edge e:g.allEdges())
      if (e.weight() == 0) 
        g.removeEdge(e.from(), e.to());
  }

  public static int minStep(WeightedDiGraph g, List<Integer> path) {
    int minCapacity = Integer.MAX_VALUE;
    for (int i = 0; i < path.size() - 1; i++) {
      int u = path.get(i);
      int v = path.get(i+1);
      minCapacity = Math.min(g.edgeWeight(u, v), minCapacity);
    }
    return minCapacity;
  }

  public static WeightedDiGraph initialResid(WeightedDiGraph cap) {
    return new WeightedDiGraph(cap);
  }

  public static WeightedDiGraph initialFlow(WeightedDiGraph cap) {
    List<WeightedDiGraph.Edge> edges = new ArrayList<>();
    for (WeightedDiGraph.Edge edge : cap.allEdges()) {
      edges.add(new WeightedDiGraph.Edge(edge.from(), edge.to(), 0));
      edges.add(new WeightedDiGraph.Edge(edge.to(), edge.from(), 0));
    }
    return new WeightedDiGraph(cap.nbOfVertices(), edges);
  }

  public static void updateFlow(WeightedDiGraph flow,
                                WeightedDiGraph resid,
                                List<Integer>   path,
                                int             benefit) {
    for (int i = 0; i < path.size() - 1; i++) {
      int from = path.get(i);
      int to = path.get(i + 1);
      addCost(resid, from, to, -benefit);
      if (resid.edgeWeight(from, to) == 0) resid.removeEdge(from, to);
      addCost(flow, from, to, benefit);
      addCost(flow, to, from, -benefit);
      addCost(resid, to, from, benefit);
    }
  }

  private static void addCost(WeightedDiGraph g, int from, int to, int delta) {
    int weight = g.isEdge(from, to) ? g.edgeWeight(from, to) : 0;
    // Update edge
    g.removeEdge(from, to);
    g.putEdge(from, to, weight + delta);
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  public static void main(String[] args) {
    int nVertices = 6;  //int nEdges = 12;
    final int A=0, B=1, C=2, D=3, E=4, F=5;
    int[] srcs  = {A, A, A, B, B, D, D, D, D, E, F, F };
    int[] dsts  = {B, C, F, F, C, A, B, C, E, A, D, E };
    int[] costs = {12,6, 14,1, 7, 9, 3, 2, 4, 5, 10,11};
    
    WeightedDiGraph g = new WeightedDiGraph(nVertices, srcs, dsts, costs);
    System.out.println("                Input Graph: " + g);
    
    int source=0, sink=2;
    System.out.println("              Source vertex: "+ source);
    System.out.println("                Sink vertex: "+ sink);
    MaxFlowResult result = maxFlow(g, source, sink);
    System.out.println("               Maximal flow: "+ result.flow());
    System.out.println("           Total throughput: "+ result.throughput());
    System.out.println("Source-group in the min-cut: "+ groupFromArray(result.isInSourceGroup()));    
  }
  
  static List<Integer> groupFromArray(boolean[] t) {
    List<Integer> res = new LinkedList<>();
    for(int i=0; i<t.length; i++)
      if (t[i]) res.add(i);
    return res;
  }

}
