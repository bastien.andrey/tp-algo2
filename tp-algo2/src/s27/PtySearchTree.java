package s27;
import java.awt.Point;
import java.util.*;

public class PtySearchTree {
  private static final Comparator<Point> VERTICALLY = 
      Comparator.<Point>comparingInt(a->a.y).thenComparingInt(a->a.x);
  private static final Comparator<Point> HORIZONTALLY = 
      Comparator.<Point>comparingInt(a->a.x).thenComparingInt(a->a.y);
  //============================================================
  record PtySearchElt(Point ptWithXmedian, Point ptWithYmin) {
    @Override public String toString() {
      return "" + ptWithXmedian.x + "-" + ptWithYmin.x + "-" + ptWithYmin.y;
    }
  }
  //============================================================
  private final BTree<PtySearchElt> tree;

  public PtySearchTree(Point[] points) {
    List<Point> v = new ArrayList<>();
    v.addAll(Arrays.asList(points));
    v.sort(VERTICALLY);
    tree = build(v);
  }

  public List<Point> search(int xFrom, int xTo, int yTo) {
    List<Point> v = new ArrayList<>();
    search(tree.root(), xFrom, xTo, yTo, v);
    return v;
  }

  public String toString() {
    return tree.toReadableString();
  }

  private static List<Point> pointsBefore(List<Point> xP, Point xMid) {
    List<Point> v = new ArrayList<>();
    for(Point p: xP) {
      if(HORIZONTALLY.compare(p, xMid) <= 0)
        v.add(p);
    }
    return v;
  }

  private static List<Point> pointsAfter(List<Point> xP, Point xMid) {
    List<Point> v = new ArrayList<>();
    for(Point p: xP) {
      if(HORIZONTALLY.compare(p, xMid) > 0)
        v.add(p);
    }
    return v;
  }

  private static void search(BTreeItr<PtySearchElt> z, int xFrom, int xTo, int yTo, 
                             List<Point> result) {
    if (z.isBottom()) return;
    PtySearchElt e = z.consult();
    if (e.ptWithYmin.y > yTo) return;

    if (e.ptWithYmin.x >= xFrom && e.ptWithYmin.x <= xTo)
      result.add(e.ptWithYmin);

    if (xTo >= e.ptWithXmedian.x)
      search(z.right(), xFrom, xTo, yTo, result);
    if (xFrom <= e.ptWithXmedian.x)
      search(z.left(), xFrom, xTo, yTo, result);
  }

  private static BTree<PtySearchElt> build(List<Point> vertSorted) {
    assert (isMonoton(vertSorted, VERTICALLY));
    BTree<PtySearchElt> r = new BTree<>();
    BTreeItr<PtySearchElt> ri = r.root();
    if (vertSorted.isEmpty()) return r;
    Point minY = vertSorted.get(0);
    int n = vertSorted.size();
    List<Point> horiSorted = new ArrayList<>(vertSorted);
    horiSorted.sort(HORIZONTALLY);
    Point midX = horiSorted.get(n/2);
    ri.insert(new PtySearchElt(midX, vertSorted.remove(0)));
    System.out.println("size: " + n + "/midx: " + midX);
    // partitioning
    List<Point> left = pointsBefore(vertSorted, midX);
    List<Point> right = pointsAfter(vertSorted, midX);
    left.sort(VERTICALLY);
    right.sort(VERTICALLY);

    ri.insert(new PtySearchElt(midX, minY));
    ri.left().paste(build(left));
    ri.right().paste(build(right));

    return r;
  }

  private static boolean isMonoton(List<Point> v, Comparator<Point> c) {
    boolean ok = true;
    for (int i=1; i<v.size(); i++) {
      ok = ok && c.compare(v.get(i-1), v.get(i)) <= 0;
    }
    return ok;
  }
}
