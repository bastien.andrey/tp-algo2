package s27;

import java.awt.Point;
import java.util.Random;

import static java.awt.geom.Point2D.distance;

// The challenge is to reach the "hacked it!" line… 
public class ClosestPairBrainteaser {
  public static Point[] naiveClosest(Point[] pts) {
    checkIntegrity(pts);
    int n = pts.length;
    if (n < 2) throw new IllegalArgumentException();
    Point[] res = new Point[2];
    double d = Double.POSITIVE_INFINITY;
    for (int i=0; i<n; i++) {
      for (int j=i+1; j<n; j++) {
        Point pi = pts[i], pj = pts[j];
        double thisDist = distance(pi.getX(), pi.getY(), pj.getX(), pj.getY());
        if (thisDist < d) {
          res[0] = pi; 
          res[1] = pj;
          d = thisDist;
        }
      }
    }
    if (res[0] == null)
      throw new IllegalStateException("Well, I hacked it!");
    return res;
  }

  private static Random rnd = new Random();
  private static void checkIntegrity(Point[] pts) {
    for(Point p: pts) {
      var xOrig = p.getX();
      var yOrig = p.getY();
      int k = rnd.nextInt(10);
      for(int i=0; i<k; i++) {
        int x = rnd.nextInt(), y = rnd.nextInt();
        p.setLocation(x, y);
        if(p.getX() != x || p.getY() != y) 
          throw new Error("unacceptable implementation...");
      }
      p.setLocation(xOrig, yOrig); // restore original values;
    }
  }
}
