package s22;
import java.awt.Point;

public class Geom {

  public static int signedArea(Point p1, Point p2, Point p3) {
    return (p2.x-p1.x)*(p3.y-p1.y) - (p3.x-p1.x)*(p2.y-p1.y);
    // negative if clockwise; twice the area of the triangle p1-p2-p3
  }

  public static int ccw(Point p1, Point p2, Point p3) {
    int a = signedArea(p1, p2, p3);
    if (a < 0) return -1;
    if (a > 0) return +1;
    return 0;
  }

  public static boolean intersect(Segm s0, Segm s1) {
    // TODO - Adapt to handle missing cases (namely 3-4 aligned points)
    return ((ccw(s0.from(), s0.to(), s1.from())
            *ccw(s0.from(), s0.to(), s1.to())) <= 0)
        && ((ccw(s1.from(), s1.to(), s0.from())
            *ccw(s1.from(), s1.to(), s0.to())) <= 0);
  }

  /**
   * @author LucyJuza
   * @param query the point you want to know wether it is in the left angle of a,b,c or not
   * @param a the a point
   * @param b the b point
   * @param c the c point
   * @return true if the query point is in the left angle of "a,b,c"
   */
  public static boolean isInLeftAngle(Point query, Point a, Point b, Point c) {
    return ccw(a,b,c) < 0 ?
            (ccw(a,b,query) > 0) || (ccw(b,c,query) > 0):
            (ccw(a,b,query) > 0) && (ccw(b,c,query) > 0);
  }

  public static boolean isInTriangle(Point query, Point a, Point b, Point c) {
    return true; // TODO - A COMPLETER..
  }

  /**
   * @author LucyJuza
   * @param p the point to be tested
   * @param s the segment
   * @return True if p is on (inside) the segment s, false otherwise.
   */
  public static boolean isOnSegment(Point p, Segm s) {
    Point A = s.from();
    Point B = s.to();
    if (Math.max(Math.max(A.x,B.x),p.x) == p.x && p.x != Math.max(A.x,B.x)) return false;
    if (Math.max(Math.max(A.y,B.y),p.y) == p.y && p.y != Math.max(A.y,B.y)) return false;
    if (Math.min(Math.min(A.x,B.x),p.x) == p.x && p.x != Math.min(A.x,B.x)) return false;
    if (Math.min(Math.min(A.y,B.y),p.y) == p.y && p.y != Math.min(A.y,B.y)) return false;

    if (B.x -A.x != 0) {
      double k1 = (double) (p.x - A.x) / (B.x - A.x);
      if (B.y - A.y != 0){
        double k2 = (double) (p.y - A.y) /(B.y - A.y);
        return k1==k2;
      }else {
        return true; // p.y-A.y == 0 always true here
      }
    }else{
      return true; // p.x - A.x == 0 always ture here
    }
  }

  public static boolean isInCcwOrder(Point [] simplePolygon) {
    return true; // TODO - A COMPLETER..
  }

  public static boolean isInPolygon(Point [] polyg, Point p) {
    return true; // TODO - A COMPLETER..
  }

  public static void main(String[] args) {
    s22.CG.main(args);
  }

}
