package s22;
import java.awt.Point;

public record Segm(Point from, Point to) {}
