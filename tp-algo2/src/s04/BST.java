package s04;

import java.util.Arrays;

public class BST<E extends Comparable<E>> {
  protected BTree<E> tree;
  protected int   crtSize;  // currentSize

  public BST() {
    tree = new BTree<>();
    crtSize = 0;
  }

  public BST(E[] tab) {  // PRE sorted, no duplicate
    tree = optimalBST(tab, 0, tab.length-1);
    crtSize = tab.length;
  }

  /** returns where e is, or where it should be inserted as a leaf */
  protected  BTreeItr<E> locate(E e) {
    BTreeItr<E> itr = new BTreeItr<>(tree);

    while (!itr.isBottom()) {
      // element is already in tree, return its location
      if (e.compareTo(itr.consult()) == 0) return itr;

      if (e.compareTo(itr.consult()) < 0) {
        itr = itr.left();
      }
      else {
        itr = itr.right();
      }
    }
    return itr;
  }

  public void add(E e) {
    BTreeItr<E> location = locate(e);
    // Check if element already in tree
    if (!locate(e).isBottom()) return;

    crtSize++;
    location.insert(e);
  }

  public void remove(E e) {
    BTreeItr<E> location = locate(e);

    // Check if element not in tree
    if (locate(e).isBottom()) return;

    while (location.hasRight()) {
      location.rotateLeft();
      location = location.left();
    }
    location.paste(location.left().cut());
    crtSize--;
  }

  public boolean contains(E e) {
    BTreeItr<E> ti = locate(e);
    return ! ti.isBottom();
  }

  public int size() {
    return crtSize;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public E minElt() {
    BTreeItr<E> itr = new BTreeItr<>(tree);
    itr = itr.rightMost().up();
    return itr.consult();
  }

  public E maxElt() {
    BTreeItr<E> itr = new BTreeItr<>(tree);
    itr = itr.leftMost().up();
    return itr.consult();
  }

  @Override public String toString() {
    return "" + tree;
  }

  public String toReadableString() {
    String s = tree.toReadableString();
    s += "size==" + crtSize + "\n";
    return s;
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------

  /***
   * Recursive function that aims to create a
   * @author NoeJuza
   * @param sorted the sorted array you wanna convert into a BST
   * @param left the left index of the subtree in the array
   * @param right the right index of the subtree in the array
   * @return returns the (sub)tree created between the indexes in the array
   */
  private BTree<E> optimalBST(E[] sorted, int left, int right) {
    BTree<E> r = new BTree<>();
    BTreeItr<E> ri = r.root();
    if (sorted.length==0  || left > right) return r;
    if (left == right){
      ri.insert(sorted[left]);
      return r;
    }
    int middle = ((right-left)/2) + left  ;
    ri.insert(sorted[middle]);
    ri.left().paste(optimalBST(sorted,left,middle-1));
    ri.right().paste(optimalBST(sorted,middle+1,right));
    return r;
  }
}
