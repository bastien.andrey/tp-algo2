package s14;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//======================================================================
public class LempelZiv {
  //======================================================================
  static class LzvCodeTable {
    // Here we store the data two different ways, so that we can efficiently
    // lookup using either the entryNumber, or the contents (byte array)
    private final Map<ArrayList<Byte>, Integer> map;
    private final ArrayList<byte[]> table;
    
    public LzvCodeTable() {
      table = new ArrayList<>();
      table.add(new byte[0]);              // dummy "empty string" entry
      map = new HashMap<>();
      map.put(new ArrayList<Byte>(), 0);   // dummy "empty string" entry
    }

    private int lookup(byte[] s) {
      return map.getOrDefault(asArrayList(s), -1);
    }

    private static ArrayList<Byte> asArrayList(byte[] s) {
      ArrayList<Byte> as = new ArrayList<>();
      for(byte b:s) as.add(b);
      return as;
    }

    public int size() { return map.size(); }

    public boolean contains(byte[] s) {
      return map.containsKey(asArrayList(s));
    }

    public int putAndCode(byte[] s, byte c) {
      int i = lookup(s);
      byte[] z=addToByteArray(s, c);
      table.add(z);
      map.put(asArrayList(z), map.size());
      return i;  
    }

    public byte[] putAndDecode(int index, byte c) {      
      byte[] s = table.get(index);
      byte[] z=addToByteArray(s, c);
      table.add(z);
      map.put(asArrayList(z), map.size());
      return z;
    }

    public String toString() {
      String s = "";
      for (int i=0; i<table.size(); i++) {
        byte[] b = table.get(i);
        String w = "";
        for(byte c:b) w+=Character.isLetterOrDigit((char)c) ? (char)c : "-";
        s += "" + i + ": " + Arrays.toString(b) + " \"" + w + "\"\n";
      }
      return s;
    }
  }
  //======================================================================
  static class LzvItem {
    final int  entryNb;
    final byte appendedChar;
    
    public LzvItem(int entryNb, byte appendedChar) {
      this.entryNb = entryNb;
      this.appendedChar = appendedChar;
    }
    @Override public boolean equals(Object o) {
      if (o == null || this.getClass() != o.getClass()) return false;
      LzvItem oo = (LzvItem)o;
      return oo.entryNb==entryNb && oo.appendedChar==appendedChar;
    }
  }
  //======================================================================

  /**
   * @author NoeJuza
   * @param infile the path to the input file
   * @param outfile the path to the output file
   * @throws IOException an exception if a file cannot be accessed
   * codes and writes a file with Lempel-Ziv compression
   */
  public static void code(String infile, String outfile) throws IOException  {
    FileInputStream  bis  = new FileInputStream(infile);
    FileOutputStream bos = new FileOutputStream(outfile);
    PrintWriter      tos = new PrintWriter(new FileWriter(outfile+".txt"));
    LzvCodeTable t = new LzvCodeTable();
    int nextCrt = bis.read();
    byte [] prefix=new byte[0];
    while(nextCrt != -1) {
      // general idea:
      // - looking for next byte
      // - if EOF
      // - - if last iteration ended in a new sequence do nothing and stop
      // - - else -> store the current sequence again
      // - if prefix + current is not in table
      // - - store the newfound sequence and reset prefix
      // - else
      // - - define prefix as prefix + current
      byte crtByte = (byte) nextCrt;
      nextCrt = bis.read();
      byte[] newPrefix = addToByteArray(prefix, crtByte);
      int newPrefixIndex = t.lookup(newPrefix);
      if (newPrefixIndex == -1) { // Prefix doesn't exist
        LzvItem item = new LzvItem(t.lookup(prefix), crtByte);
        printItem(bos, tos, item);
        t.putAndCode(prefix, crtByte);
        newPrefix = new byte[0];
      } else if (nextCrt == -1) { // EOF reached before a new prefix, we need to add the prefix one more time
        LzvItem item = new LzvItem(t.lookup(prefix), crtByte);
        printItem(bos, tos, item);
        t.putAndCode(prefix, crtByte);
      }
      prefix = newPrefix;
    }
    bis.close();
    bos.close();
    tos.close();
    System.out.println("Code table: size = "+t.size());
    if (t.size() < 10) {
      System.out.println("Table Content:");
      System.out.println(t);
    }
  }

  public static void decode(String infile, String outfile) throws IOException {
    FileInputStream  bis = new FileInputStream(infile);
    Scanner          tis = new Scanner(new FileReader(infile+".txt"));
    FileOutputStream bos = new FileOutputStream(outfile);
    LzvCodeTable t = new LzvCodeTable();
    LzvItem item = readItem(bis, tis);
    while(item != null) {    
      byte[] s = t.putAndDecode(item.entryNb, item.appendedChar);
      bos.write(s); 
      item=readItem(bis, tis);
    }
    bis.close();
    tis.close();
    bos.close();
    System.out.println("Code table: size = "+t.size());
    if (t.size() < 10) {
      System.out.println("Table Content:");
      System.out.println(t);
    }
  }

  private static byte[] addToByteArray(byte [] t, byte b) {
    byte [] res=new byte[t.length+1];
    System.arraycopy(t, 0, res, 0, t.length);
    res[res.length-1] = b;
    return res;
  }

  private static void printItem(FileOutputStream bos, 
                                PrintWriter      tos,
                                LzvItem          item) throws IOException {
    printItemAsBinary(bos, item);  // really compressed!
    printItemAsText  (tos, item);  // useful for debugging
  }

  /**
   * @author NoeJuza
   * @param bos the output stream
   * @param item the item to be print
   * @throws IOException when the fileStream has a problem
   */
  private static void printItemAsBinary(FileOutputStream bos, LzvItem item) throws IOException {
    // printing <NbEntry1stByte><NbEntry2ndByte><AppendedChar>
    bos.write(item.entryNb >> 8); // writing the first byte of the entry number
    bos.write((byte) item.entryNb); // writing the second byte
    bos.write(item.appendedChar); // then the appended char
  }

  private static void printItemAsText( PrintWriter tos,  LzvItem item) throws IOException {
    tos.print(" "+item.entryNb); 
    tos.print(" "+item.appendedChar); 
    char c = (char) item.appendedChar;
    if (Character.isLetterOrDigit(c))
      tos.print(" \"" + c + "\"");
    else
      tos.print(" " + "-");
    tos.println();
  }

  private static LzvItem readItem(FileInputStream bis, Scanner tis) throws IOException {
    LzvItem res =  readItemFromBinary(bis);    
    LzvItem res1 = readItemFromText  (tis);    
    if (res==null && res1==null) return res;
    if((res==null ^ res1==null) || !res1.equals(res)) {
      System.out.println("Oups... binary and text formats don't agree!");
      System.exit(-1);
    }
    return res;
  }

  /**
   * @author NoeJuza
   * @param bis input stream
   * @return {@link LzvItem} a LzvItem that contains the data read, or null if EOF
   * @throws IOException throws an exception if there is a problem reading the stream
   */
  private static LzvItem readItemFromBinary(FileInputStream bis) throws IOException {
    // reading <NbEntry1stByte><NbEntry2ndByte><AppendedChar>
    int entryNbA = bis.read();
    int entryNbB = bis.read();
    int entryNb = (entryNbA << 8) | entryNbB;
    byte appendedChar = (byte) bis.read();
    if (entryNbA == -1 || entryNbB == -1 || appendedChar == -1)
      return null;
    return new LzvItem(entryNb, appendedChar);
  }

  private static LzvItem readItemFromText(Scanner tis) throws IOException {
    if (!tis.hasNext()) return null;
    int     code = tis.nextInt();
    byte crtByte = tis.nextByte();
    tis.next();  // dummy character
    LzvItem res = new LzvItem(code, crtByte);
    return res;
  }
  
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  public static void main(String [] args) {
    if (args.length != 3 ) usage();
    String cmd = args[0];
    try {
      if (cmd.equals("code"))
        code(args[1], args[2]);
      else if (cmd.equals("decode"))
        decode(args[1], args[2]);
      else
        usage();
    } catch (IOException e) {
      System.err.println(e);
      e.printStackTrace();
    }
  }
  // ------------------------------------------------------------
  private static void usage() {
    System.out.println("Usage: LempelZiv code   infile outfile");
    System.out.println("   or: LempelZiv decode infile outfile"); 
    System.exit(-1);
  }
}
