/*
* Last edited: 25.09.2023
* */
package s02;

import java.util.Arrays;

public class IntQueueChained {
  //======================================================================
  /* TODO: adapt using pseudo-pointers instead of queue node objects
   * "Memory management" code:
   * - define "memory" arrays, the NIL constant, and firstFreeCell
   * - define allocate/deallocate, with automatic array expansion
   * "User" code:
   * - modify enqueue/dequeue/..., keeping the same logic/algorithm
   * - test
   */
  private final int DEFAULT_MEM_SIZE = 3; // describes the number of elt the first array created can hold (size doubles when exceeding capacity)
  private final int NIL = -1; // constant used to detect extremes
  private int[] arrElts; // array used to store the values of the chained queue
  private int[] arrNexts; // array used to store the next chained node in the current data
  private int  iFirstFreeMemCell = 0; //describes the first free "memory" cell which is used to add a new reservation
  private int iFront = NIL; // used to point to the index of the first element of the queue (FIFO)
  private int iBack = NIL; // used to point to the index of the last added element of the queue

  /**
   * @author NoeJuza
   * @param elt int you want to store at "next" index in memory
   * @param next index of where the element should link to
   * @return the index at which the character has been added in memory
   */
  private int allocate(int elt, int next){
    if (iFirstFreeMemCell == NIL){
      // double the size of the arrays routine
      int previousSize =  arrNexts.length;
      arrElts = Arrays.copyOf(arrElts, arrElts.length*2);
      arrNexts = Arrays.copyOf(arrNexts, arrNexts.length*2);
      iFirstFreeMemCell = previousSize;
      for (int i = previousSize; i < arrNexts.length-1; i++) {
        arrNexts[i] = i+1;
      }
      arrNexts[arrNexts.length-1] = -1;
    }
    int i = iFirstFreeMemCell;
    iFirstFreeMemCell = arrNexts[i];
    arrElts[i] = elt;
    arrNexts[i] = next;
    return i;
  }

  /**
   * @author NoeJuza
   * @param i index of the element you want to deallocate
   */
  private void deallocate(int i){
    arrNexts[i] = iFirstFreeMemCell;
    iFirstFreeMemCell = i;
    arrElts[i] = 0; // null is not a possible value with ints so we set it at 0
  }
  //======================================================================
  /*static class QNode {
    final int    elt;
    QNode next = null;

    public QNode(int e) { elt = e; }
  }*/
  //======================================================================
  //private QNode front;
  //private QNode back;

  public IntQueueChained() {
    arrElts = new int[DEFAULT_MEM_SIZE];
    arrNexts = new int[DEFAULT_MEM_SIZE];
    for (int i = 0;i < DEFAULT_MEM_SIZE-1; i++) {
      arrNexts[i] = i+1;
    }
    arrNexts[DEFAULT_MEM_SIZE-1] =-1;
  }

  /**
   * @author NoeJuza
   * @param elt element to enqueue
   */
  public void enqueue (int elt) {
    int allocatedSpot = allocate(elt,-1);
    if (iBack==NIL){
      iFront = allocatedSpot;
    }else{
      arrNexts[iBack] = allocatedSpot;
    }
    iBack = allocatedSpot;
  }

  public boolean isEmpty() {
    return iBack == NIL;
  }

  public int consult() {
    return arrElts[iFront];
  }

  public int dequeue() {
    int i = iFront;
    int e = arrElts[iFront];
    if (iFront == iBack){
      iFront = NIL;
      iBack = NIL;
    }else{
      iFront = arrNexts[iFront];
    }
    deallocate(i);
    return e;
  }
}
