package s09;

// ------------------------------------------------------------ 
public class StringSearching {
  static int HASHER = 301; // Maybe also try with 7 and 46237
  static int BASE   = 256; // Please also try with 257

  static int firstFootprint(String s, int len) {
    int hash = 0;
    for (int i = 0; i < len; i++)
      hash = powerMod(BASE, hash, s.charAt(i), HASHER);
    return hash;
  }

  // must absolutely be O(1)
  static int nextFootprint(int previousFootprint, char dropChar, char newChar, int coef) {
    int h = previousFootprint;
    h = (h - dropChar * coef);
    h = (BASE * h);
    h = (h + newChar) % HASHER;
    return h;
  }
  // ---------------------

  static int powerMod(int w, int x, int y, int z) {
    return ((w%z)*(x%z)+(y%z))%z;
  }

  static int powerMod(int x, int y) {
    int coef = 1;
    while (y > 0) {
      if (y % 2 != 0)
        coef = (coef * x) % HASHER;
      x = (x * x) % HASHER;
      y /= 2;
    }
    return coef;
  }

  /**
   * Rabin-Karp algorithm
   * @param t text
   * @param p pattern
   * @return index of the first pattern occurrence in text or return -1 if not in text
   */
  public static int indexOf_rk(String t, String p) {
    int n = t.length();
    int m = p.length();
    int hashPattern;
    int hashText;

    int coef = powerMod(BASE, m - 1);

    hashPattern = firstFootprint(p, p.length());
    hashText = firstFootprint(t, p.length());

    // Match pattern
    int windowSize = n - m;
    int j;
    for (int i = 0; i <= windowSize; i++) {
      if (hashPattern == hashText) {
        // Verify each character
        for (j = 0; j < m; j++) {
          if (t.charAt(i + j) != p.charAt(j))
            break;
        }

        if (j == m)
          return i;
      }
      if (i < windowSize) {
        hashText = nextFootprint(hashText, t.charAt(i), t.charAt(i + m), coef);
        if (hashText < 0) hashText += HASHER;
      }
    }
    return -1;
  }
}
