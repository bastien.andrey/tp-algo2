package s09;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EditDistanceTestJU {
    @Test
    public void testEditDistancePattern1() {
        String s1 = "abcde";
        String s2 = "bgdfe";
        int value = EditDistance.levenshtein(s1, s2);
        assertEquals(3, value);
    }

    @Test
    public void testEditDistancePattern2() {
        String s1 = "kitten";
        String s2 = "sitting";
        int value = EditDistance.levenshtein(s1, s2);
        assertEquals(3, value);
    }
}
