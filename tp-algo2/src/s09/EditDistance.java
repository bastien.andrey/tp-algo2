package s09;

public class EditDistance {
    public static int levenshtein(String s1, String s2) {
        // Create a matrix n+1 x m+1
        int[][] matrix = new int[s1.length() + 1][s2.length() + 1];

        // Fill first row and first column
        for (int i = 0; i <= s1.length(); i++)
            matrix[i][0] = i;

        for (int j = 0; j <= s2.length(); j++)
            matrix[0][j] = j;

        // Apply edit distance
        for (int i = 1; i <= s1.length(); i++) {
            for (int j = 1; j <= s2.length(); j++) {
                int value;
                if (s1.charAt(i - 1) == s2.charAt(j - 1)) value = matrix[i-1][j-1];
                else value = 1 + Math.min(Math.min(matrix[i][j-1], matrix[i-1][j]), matrix[i-1][j-1]);
                matrix[i][j] = value;
            }
        }

        return matrix[s1.length()-1][s2.length()-1];
    }
}
