package s12.ex01;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Random;



public class FifoQueueJU {
    @Test
    public void testSimple() {
        FifoQueue<String> a =  new FifoQueue<String>();
        a.enqueue("a");
        assertEquals("a", a.consultOldest());
        a.enqueue("b");
        assertEquals("Shit hit the fan very bad","a", a.consultOldest());
        a.enqueue("c");
        a.enqueue("d");
        a.enqueue("e");
        String deq = a.dequeue();
        System.out.println(deq);
        assertEquals("Oof, wrong first element", "a", deq);
        a.enqueue("a");
        assertEquals("Oof, wrong oldestElement once removed last oldestElement element", "b", a.consultOldest());
        while (!a.isEmpty()){
            System.out.println(a.dequeue());
        }
    }
    @Test
    public void testRandomString(){
        FifoQueue<String> myQueue = new FifoQueue<>();
        Random rng = new Random();
        String[] tblStrings = new String[rng.nextInt(100)];
        for (int i = 0; i < tblStrings.length; i++) {
            tblStrings[i] = ""+(char)(rng.nextInt(26) + 'a');
        }
        System.out.println(Arrays.toString(tblStrings));
        for (String string : tblStrings) {
            myQueue.enqueue(string);
        }
        assertEquals("wrong first element",myQueue.consultOldest(),tblStrings[0]);
        for (String string : tblStrings) {
            String dequeued = myQueue.dequeue();
            System.out.println(dequeued);
            assertEquals("Wrong ellement was dequeued",dequeued , string);
        }
    }
    @Test
    public void testOneElt(){
        FifoQueue<String> myQueue = new FifoQueue<>();
        myQueue.enqueue("a");
        assertEquals("a",myQueue.consultOldest());
        assertEquals("a",myQueue.dequeue());
    }
}
