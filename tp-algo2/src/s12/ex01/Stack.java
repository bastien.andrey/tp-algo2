
package s12.ex01;

public class Stack<E> {
  java.util.Stack<E> stack;
  public Stack() {
    stack = new java.util.Stack<>();
  }
  public void push(E elt) {
    stack.push(elt);
  }
  public E pop() {
    return stack.pop();
  }
  public E top() {
    return stack.peek();
  }
  public boolean isEmpty() {
    return stack.isEmpty();
  }
}
