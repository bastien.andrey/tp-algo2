package s12.ex01;
// Implement this class, with amortized O(1) for each operation; 
// The only collection available is Stack (no array, no chaining nodes etc.)
public class FifoQueue<E> {
  private Stack<E> stackLIFO; // expected behavior of Stack, used to "enqueue" new elements
  private Stack<E> stackFIFO; // "FIFO" stack used to "dequeue" elements
  private E oldestElement; // used to store the oldest element, updated on dequeue and enqueue when empty
  boolean currentStack = false; // default to false, meaning we use stack LIFO, we are in "enqueue mode"

  /***
   * @author NoeJuza
   * constructor, used to initialize the two stacks;
   */
  public FifoQueue() {
    stackLIFO = new Stack<E>();
    stackFIFO = new Stack<E>();
  }

  /***
   * @author NoeJuza
   * @param elt element to be added to the queue
   */
  public void enqueue(E elt) {
    if (stackFIFO.isEmpty() && stackLIFO.isEmpty()) oldestElement = elt;
    stackLIFO.push(elt);
  }

  /***
   * @author NoeJuza
   * @return the dequeued ellement
   */
  public E dequeue() {
    if(stackFIFO.isEmpty()){
      if (stackLIFO.isEmpty()) throw new UnsupportedOperationException("cannot dequeue empty queue");
      reverseStack(stackLIFO,stackFIFO);
      currentStack = true;
    }
    E res = stackFIFO.pop();
    if (!stackFIFO.isEmpty()){
      oldestElement = stackFIFO.top();
    }else{
      oldestElement = null;
    }
    return res;
  }

  /***
   * @author NoeJuza
   * @return the oldest element (next in line to be dequeued)
   */
  public E consultOldest() {
    if (isEmpty()) throw new UnsupportedOperationException("Current queue is empty, can't get oldest element");
    return oldestElement;
  }

  /***
   * @author NoeJuza
   * @return true if queue is empty, false if queue is not empty
   */
  public boolean isEmpty() {
    return currentStack ? stackFIFO.isEmpty() : stackLIFO.isEmpty();
  }

  /***
   * @author NoeJuza
   * @param stack the stack to be reversed in the other one.
   * @param toBeReversedIn the stack that will hold the reversed data.
   */
  private void reverseStack(Stack<E> stack,Stack<E> toBeReversedIn){
    if (!toBeReversedIn.isEmpty()) throw new IllegalArgumentException("toBeReversedIn should be empty");
    while(!stack.isEmpty()) toBeReversedIn.push(stack.pop());
  }
}