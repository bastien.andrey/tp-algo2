package s12.ex06;

public final class ReversableLinkedList<E> implements IList<E> {
  ListNode<E> first, last;
  int size;
  
  public ReversableLinkedList() {
    size = 0;
    first = null;
    last = null;
  }
  
  @Override
  public int size() {
    return size;
  }
  
  @Override
  public IListItr<E> iterator() {
    return new ReversableLinkedListItr<>(this);
  }
  
  @Override public String toString() {
    String s = "[";
    IListItr<E> li = iterator();
    while(!li.isLast()) {
      s += " " + li.consultAfter();
      li.goToNext();
    }
    return s + " ]";
  }
  
}
