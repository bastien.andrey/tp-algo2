package s12.ex06;

// Similar to the specification we used in Algo1.  
// Every operation in O(1) worst-case CPU.
public final class ReversableLinkedListItr<E> implements IListItr<E> {
  final ReversableLinkedList<E> reversableList;
  ListNode<E> left, right;

  ReversableLinkedListItr(ReversableLinkedList<E> theList) {
    reversableList = theList;
    goToFirst();
  }

  @Override
  public void insertAfter(E e) {
    ListNode<E> listCheck;
    if (reversableList.isEmpty()) {
      listCheck = new ListNode<>(e, null, null);
      reversableList.first = listCheck;
      reversableList.last = listCheck;
    } else if (isLast()) {
      listCheck = new ListNode<>(e, left, null);
      left.next = listCheck;
      reversableList.last = listCheck;
    } else if (isFirst()) {
      listCheck = new ListNode<>(e, null, right);
      right.prev = listCheck;
      reversableList.first = listCheck;
    } else {
      listCheck = new ListNode<>(e, left, right);
      left.next = listCheck;
      right.prev = listCheck;
    }
    right = listCheck;
    reversableList.size++;
    assert consultAfter() == e;
  }

  @Override
  public void removeAfter() { 
    if(isLast()) throw new IllegalStateException("can't removeAfter when isLast");
    if (isFirst()) {
      reversableList.first = left.next;
      right = reversableList.first;
      if (isLast()) {
        reversableList.last = null;
      } else {
        right.prev = null;
      }
    } else if (right.next == null) {
      left.next = null;
      right = null;
      reversableList.last = left;
    } else {
      right = right.next;
      left.next = right;
      right.prev = left;
    }
    reversableList.size--;
  }

  @Override
  public E consultAfter() {
    if(isLast()) throw new IllegalStateException("can't consultAfter when isLast");
    return right.elt;
  }

  @Override
  public void goToNext() { 
    if(isLast()) throw new IllegalStateException("can't goToNext when isLast");
    left = right;
    right = right.next;
  }

  @Override
  public void goToPrev() {
    if(isFirst()) throw new IllegalStateException("can't goToPrev when isFirst");
    right = left;
    left = left.prev;
  }

  @Override
  public void goToFirst() {
    right = reversableList.first;
    left = null;
  }
  
  @Override
  public void goToLast() {
    left = reversableList.last;
    right = null;
  }
  
  @Override
  public boolean isFirst() { 
    return left == null;
  }
  
  @Override
  public boolean isLast()  {
    return right == null;
  }

  @Override
  public void reverseBefore() {
    // Il faut redéfinir les liens du premier noeud de la liste et les liens du noeud gauche de l'itérateur
    // et aussi changer l'élément fisrt de la liste avec le nouvel élément
    ListNode<E> leftNode = left;
    ListNode<E> firstNode = reversableList.first;
    // L'élément first devient l'élément gauche de l'itérateur
    left = reversableList.first;
    leftNode.prev.next = reversableList.first;
    left.next = leftNode.next;
    // L'élément gauche de l'itérateur devient l'élément first
    reversableList.first = leftNode;
    firstNode.next.prev = leftNode;
    reversableList.first = firstNode.next;
  }
}
