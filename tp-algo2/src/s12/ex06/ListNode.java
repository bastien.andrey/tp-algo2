package s12.ex06;

public class ListNode<E> {
    E elt;
    ListNode<E> prev, next;
    public ListNode (E elt, ListNode<E> prev, ListNode<E> next) {
        this.elt = elt;
        this.next = next;
        this.prev = prev;
    }
}
