package s12.ex14;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Propagation {
  // Worst-case O(n) in CPU, O(1) in RAM

  /**
   * @author NoeJuza
   * @param t the matrix you want to mutate to be propagated
   * @param e value to be propagated
   */
  public static void propagate2D(int[][] t, int e) {
    int col = -1;
    for (int i = 0; i < t.length; i++) {
      boolean foundE = false;
      for (int j = 0; j < t[i].length; j++) {
        System.out.println("entering [" + i + "," + j + "] col is: " + col );
        if (t[i][j] == e){
          foundE = true;
          col = j;
          if (i != t.length-1) break;
        }
        if (col == j){
          t[i][j] = e;
          for (int k = i; k >= 0; k--) {
            t[k][j] = e;
          }
        }
      }
      if (foundE){
        Arrays.fill(t[i],e);
      }
    }
  }

  // Worst-case O(n) in CPU, O(1) in RAM
  public static void propagate3D(int[][][] t, int e) {
    // TODO (s12.ex14b)
  }
  
  static int[][] deepCopy(int[][] t) {
    int n=t.length, m=t[0].length;
    int[][] res=new int[n][m];
    for(int i=0; i<n; i++)
      for(int j=0; j<m; j++)
        res[i][j] = t[i][j];
    return res;
  }
  
  static String asString(int[][] t) {
    return Arrays.stream(t)
        .map(z -> Arrays.toString(z))
        .collect(Collectors.joining("\n"))  + "\n";
  }

  static void demo() {
    int[][] t = {
        {1,3,2,4},
        {2,8,1,3},
        {2,5,3,2},
        {4,4,4,8}
    };
    int[][] r = {
        {1,8,2,8},
        {8,8,8,8},
        {2,8,3,8},
        {8,8,8,8}
    };

    System.out.println(asString(t));
    propagate2D(t, 8);
    System.out.println(asString(t));
    if(!asString(t).equals(asString(r)))
      System.out.println("Something's wrong...");
  }
  
  public static void main(String[] args) {
    demo();
  }
}
