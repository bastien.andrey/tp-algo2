
package s12.ex07;

import java.io.*;

public class MissingIntInFile {
  // s12.ex7
  public static int oneOfTheMissingInts(String filename) {
    int oneOfMissingInt = 0;
    File intFile = new File(filename);
    BufferedReader br = null;

    try {
      br = new BufferedReader(new FileReader(intFile));
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
      System.exit(-1);
    }

    // Trouver les bornes (min et max)
    String line;
    int min = Integer.MAX_VALUE;
    int max = Integer.MIN_VALUE;
    try {
      while ((line = br.readLine()) != null) {
        int value = Integer.parseInt(line);
        if (value < min) min = value;
        else if (value > max) max = value;
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    int intSize = 32;
    for (int i = 0; i < intSize; i++) {
      int mask = 1 << i;
      int majority = 0;

      try {
        while ((line = br.readLine()) != null)
          if ((Integer.parseInt(line) & mask) == mask) majority++;
      }
      catch (IOException e) {
        e.printStackTrace();
      }

      if (majority < Math.pow(2, intSize - i - 1))
        oneOfMissingInt = oneOfMissingInt | mask;
    }

    return oneOfMissingInt;
  }
  
  // s12.ex7b: PRE: the file has all numbers between 0-n except a single one
  public static int singleMissingInt(String filename, int n) {
    File intFile = new File(filename);
    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(intFile));
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
      System.exit(-1);
    }

    int sumOfAll = 0;
    for (int i = 0; i <= n; i++) sumOfAll += i;

    int sum = 0;
    String line;
    try {
      while ((line = br.readLine()) != null)
        sum += Integer.parseInt(line);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    return sumOfAll - sum;
  }

  public static void main(String[] args) {
    int missing = singleMissingInt("tp-algo2/src/s12/ex07/oneMissingInt.txt", 20);
    System.out.println("The missing int is: " + missing);
  }
}
