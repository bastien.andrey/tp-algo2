package s12.ex09;

// Every operation as efficient as removeMin() in a standard min-Heap
public class BetterHeap<E extends Comparable<E>> {

  static class HeapNode<E> {
    E elt;
    HeapNode<E> right, left, parent;

    public HeapNode(E elt, HeapNode<E> right, HeapNode<E> left, HeapNode<E> parent) {
      this.elt = elt;
      this.right = right;
      this.left = left;
      this.parent = parent;
    }
  }

  HeapNode<E> root;

  public BetterHeap() { 
    root = null;
  }

  public void add(E e) {
    // TODO
    if (root == null) {
      root = new HeapNode<>(e, null, null, null);
    }
    else {

    }
  }
  public E removeMin() {
    return null; // TODO
  }

  public E removeMax() {
    return null; // TODO
  }

  public E min() {
    return null; // TODO
  }

  public E max() {
    return null; // TODO
  }

  public boolean isEmpty() {
    return root == null;
  }
}