
package s12.ex03;

import java.util.EmptyStackException;
import java.util.Stack;

// Each operation in O(1) CPU worst-case
public class BetterStack {
  private Stack<Float> stack =  new Stack<Float>();
  private Stack<Float> minStack = new Stack<Float>();
  private Stack<Float> maxStack = new Stack<Float>();

  /***
   * used to push a new element to the stack
   * @author NoeJuza
   * @param e the element you want to push on top of the stack
   */
  public void push(float e) {
    if (isEmpty()){
      maxStack.push(e);
      minStack.push(e);
    }else{
      if (e > consultMax()) maxStack.push(e);
      if (e < consultMin()) minStack.push(e);
    }
    stack.push(e);
  }

  /***
   * Removes and return the element at the top of the stack
   * @author NoeJuza
   * @return the element that was removed of the stack
   * @throws EmptyStackException when stack is empty
   */
  public float pop() {
    if (stack.isEmpty()) throw new EmptyStackException();
    float res = stack.pop();
    if (!stack.isEmpty()){
      if (res == consultMax()) maxStack.pop();
      if (res == consultMin()) minStack.pop();
    }
    return res;
  }

  /***
   * Used to check the minimal value of the stack
   * @author NoeJuza
   * @return the minimal value currently in the stack
   * @throws EmptyStackException when stack is empty
   */
  public float consultMin() {
    if (stack.isEmpty()) throw new EmptyStackException();
    return minStack.peek();
  }
  /***
   * Used to check the maximal value of the stack
   * @author NoeJuza
   * @return the maximal value currently in the stack
   * @throws EmptyStackException when stack is empty
   */
  public float consultMax() {
    if (stack.isEmpty()) throw new EmptyStackException();
    return maxStack.peek();
  }

  /***
   * Used to check if the stack is empty (no elts)
   * @author NoeJuza
   * @return true if the stack is empty, false otherwise
   */
  public boolean isEmpty() {
    return stack.isEmpty();
  }
}
