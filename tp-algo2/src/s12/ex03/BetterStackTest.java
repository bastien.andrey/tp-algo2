package s12.ex03;
import org.junit.Test;
import static org.junit.Assert.*;
public class BetterStackTest {
    @Test
    public void testSimple(){
        BetterStack a = new BetterStack();
        a.push(0);
        assertEquals("Shit hit the fan really bad", 0, a.consultMin(), 0.0);
        assertEquals("Shit hit the fan really bad", 0, a.consultMax(), 0.0);
        a.push(10);
        assertEquals("max is not updated correctly", 10, a.consultMax(), 0.0);
        assertEquals("min is updated when it shouldn't", 0, a.consultMin(), 0.0);
        a.push(15);
        assertEquals("max is not updated correctly", 15, a.consultMax(), 0.0);
        assertEquals("pop behaves weirdly", 15, a.pop(), 0.0);
        assertEquals("max is not updated correctly", 10, a.consultMax(), 0.0);
        a.push(-1);
        assertEquals("min is not updated correctly", -1, a.consultMin(), 0.0);
        assertEquals("max is not updated correctly", 10, a.consultMax(), 0.0);
        assertEquals("pop behaves weirdly", -1, a.pop(), 0.0);
        assertEquals("min is not updated correctly", 0, a.consultMin(), 0.0);
        assertEquals("max is not updated correctly", 10, a.consultMax(), 0.0);
        assertEquals("pop behaves weirdly", 10, a.pop(), 0.0);
        assertEquals("min has a really weird behavior", 0, a.consultMin(), 0.0);
        assertEquals("max is not updated correctly", 0, a.consultMax(), 0.0);
        assertEquals("pop behaves weirdly", 0, a.pop(), 0.0);
        assertTrue("stack is not considered empty when it is", a.isEmpty());
        try {
            float min = a.consultMin();
            throw new Error("shouldn't be able to consult min when empty");
        }catch (Exception e){
            System.out.println("access to min on empty stack has thrown as expected");
        }
        try {
            float max = a.consultMax();
            throw new Error("shouldn't be able to consult max when empty");
        }catch (Exception e){
            System.out.println("access to max on empty stack has thrown as expected");
        }
    }
}
