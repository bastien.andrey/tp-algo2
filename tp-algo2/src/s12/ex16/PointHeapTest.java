package s12.ex16;

import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import static org.junit.Assert.*;
public class PointHeapTest {
    @Test
    public void testSimple() {
        PointHeap myPointHeap = new PointHeap();
        //most "trivial™️" test
        Point a = new Point(15,10);
        myPointHeap.add(a);
        assertEquals("\uD83D\uDCA9 hit the fan",myPointHeap.removeMinX(),a);
        //less trivial test, still a "basic case"
        myPointHeap.add(a);
        Point b = new Point(14, 11);
        myPointHeap.add(b);
        assertEquals("\uD83D\uDCA9 hit the fan",myPointHeap.removeMinX(),b);
        myPointHeap.add(b);
        Point c = new Point(13,11);
        myPointHeap.add(c);
        assertEquals("\uD83D\uDCA9 hit the fan",myPointHeap.removeMinX(),c);
        myPointHeap.add(c);
        assertEquals("\uD83D\uDCA9 hit the fan",myPointHeap.removeMinY(),a);
        myPointHeap.add(a);
        Point d = new Point(15,9);
        myPointHeap.add(d);
        assertEquals("\uD83D\uDCA9 hit the fan",myPointHeap.removeMinY(),d);
        myPointHeap.add(d);
        //"real" test
        Random seedGen = new Random();
        int seed = seedGen.nextInt();
        Random r = new Random(400043369);
        System.out.println("seed is: " + 400043369);
        ArrayList<Point> arrLst = new ArrayList<>();
        PointHeap pointHeap = new PointHeap();
        for (int i = 0; i < 10000; i++) {
            Point rdmPoint = new Point(r.nextInt(1000), r.nextInt(1000));
            arrLst.add(rdmPoint);
            pointHeap.add(rdmPoint);
        }
        arrLst.sort(new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                if (o1.x == o2.x) return 0;
                return o1.x < o2.x ? -1 : 1;
            }
        });
        assertEquals("\uD83D\uDCA9 hit the fan",arrLst.get(0).x,pointHeap.removeMinX().x);
    }
}
