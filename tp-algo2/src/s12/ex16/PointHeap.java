package s12.ex16;

import s03.Heap;

import java.awt.Point;
import java.util.ArrayList;

// Every operation as efficient as removeMin() in a standard min-Heap
public class PointHeap{
  private final ArrayList<Point> buffer = new ArrayList<>();

  /***
   * @author NoeJuza
   * @param p point to be added in the heap
   * This method adds an element to the heap.
   */
  public void add(Point p) {
    if (isEmpty()) {
      buffer.add(p);
      return;
    }
    buffer.add(p);
    siftUp(buffer.size()-1);
  }
  private void siftDown(int i){
    int level = level(i);
    boolean ssr = shouldSiftRight(i,level);
    boolean ssdr = shouldSiftDownRight(i,level);
    boolean ssdl = shouldSiftDownLeft(i,level);
    while ( i < buffer.size()-1 && (ssdr || ssdl || ssr) ){
      if (ssdl){
        int newi = leftChild(i);
        swap(i,newi);
        i = newi;
      } else if (ssdr) {
        int newi = rightChild(i);
        swap(i,newi);
        i = newi;
      }else if (ssr){
        int newi = i+1;
        swap(i,newi);
        i = newi;
      }
      level = level(i);
      ssr = shouldSiftRight(i,level);
      ssdr = shouldSiftDownRight(i,level);
      ssdl = shouldSiftDownLeft(i,level);
    }
  }
  private boolean shouldSiftRight(int i, int level){
    return  i < buffer.size()-2 && (
      (level %2 == 0 && buffer.get(i).x > buffer.get(i+1).x )
      ||
      (level % 2 != 0 && buffer.get(i).y > buffer.get(i+1).y)
    );
  }
  private boolean shouldSiftDownLeft(int i, int level){
    return hasLeftChild(i) && (
      (level%2==0 && buffer.get(i).x > buffer.get(leftChild(i)).x)
      ||
      (level%2!=0 && buffer.get(i).y > buffer.get(leftChild(i)).y)
    );
  }
  private boolean shouldSiftDownRight(int i, int level){
    return hasRightChild(i) && (
      (level%2==0 && buffer.get(i).y > buffer.get(rightChild(i)).y)
      ||
      (level%2!=0 && buffer.get(i).x > buffer.get(rightChild(i)).x)
    );
  }
  private  void siftUp(int i){
    int level = level(i);
    boolean ssl = shouldSiftLeft(i,level);
    boolean ssu = shouldSiftUp(i,level);
    while (i > 0 && (ssl || ssu)){
      if (ssu){
        int newi = parent(i);
        swap(i,newi);
        i = newi;
      }else if (ssl){
        int newi = i-1;
        swap(i,newi);
        i= newi;
      }
      level = level(i);
      ssu = shouldSiftUp(i,level);
      ssl = shouldSiftLeft(i,level);
    }
  }

  private boolean shouldSiftLeft(int i, int level){
    return  i>1 && (
            (level %2 == 0 && buffer.get(i).x < buffer.get(i-1).x )
            ||
            (level %2 != 0  && buffer.get(i).y < buffer.get(i-1).y)
    );
  }
  private boolean shouldSiftUp(int i, int level){
    return i>0 && (
      (level %2 == 0 && (
          (leftChild(parent(i)) == i && buffer.get(i).y < buffer.get(parent(i)).y)
          ||
          (rightChild(parent(i)) == i && buffer.get(i).x < buffer.get(parent(i)).x)
        )
      )
      ||
      (level%2 != 0  &&(
          (leftChild(parent(i)) == i && buffer.get(i).x < buffer.get(parent(i)).x)
          ||
          (rightChild(parent(i)) == i && buffer.get(i).y < buffer.get(parent(i)).y)
        )
      )
    );
  }

  private int parent(int i) {
    return (i-1)/2;
  }

  private int leftChild (int i) {
    return 2*i+1;
  }

  private int rightChild(int i) {
    return 2*i+2;
  }
  private void swap(int i, int j) {
    Point aux=buffer.get(i);
    buffer.set(i, buffer.get(j));
    buffer.set(j, aux);
  }
  private int level(int i){
    if (i == 0) return 0;
    //System.out.println("level for " + i + " is " + (int) ( Math.log(i+1)/Math.log(2) ) );
    return (int) ( Math.log(i+1)/Math.log(2) ); // log(i)/log(2) = log_2(i)
  }
  private boolean hasLeftChild(int i){
    return leftChild(i) < buffer.size() &&  buffer.get(leftChild(i)) != null;
  }
  private boolean hasRightChild(int i){
    return rightChild(i) < buffer.size() && buffer.get(rightChild(i)) != null;
  }
  private boolean hasChildren(int i){
    return hasLeftChild(i) && hasRightChild(i);
  }
  public boolean isEmpty() {
    return buffer.isEmpty();
  }
  
  public Point removeMinX() {
    //the minx is guaranteed to be inside 0,1 or 2
    if (buffer.isEmpty()) return null;
    Point a = buffer.get(0);
    if (buffer.size() == 1) {
      return buffer.remove(0);
    }
    Point b = buffer.get(1);
    if (buffer.size() == 2){
      if (b.x > a.x) {
        swap(1,0);
      }
      return buffer.remove(1);
    }
    Point c = buffer.get(2);
    int min;
    if (a.x <= b.x && a.x <= c.x ) min=0;
    else if (b.x <= a.x && b.x <= c.x ) min=1;
    else min=2;
    Point minPoint = buffer.get(min);
    System.out.println("possible x values were: " + a.x + " " + b.x + " " + c.x + " and " + minPoint.x + " was selected as min");
    Point x = buffer.remove(buffer.size()-1);
    if (x != minPoint){
      buffer.set(min,x);
      siftDown(min);
    }
    return minPoint;
  }

  public Point removeMinY() {
    //the miny is guaranteed to be inside 0,1 or 2
    if (buffer.isEmpty()) return null;
    Point a = buffer.get(0);
    if (buffer.size() == 1) {
      return buffer.remove(0);
    }
    Point b = buffer.get(1);
    if (buffer.size() == 2){
      if (b.y > a.y) {
        swap(1,0);
      }
      return buffer.remove(1);
    }
    Point c = buffer.get(2);
    int min;
    if (a.y <= b.y && a.y <= c.y ) min=0;
    else if (b.y <= a.y && b.y <= c.y ) min=1;
    else min=2;
    Point minPoint = buffer.get(min);
    Point x = buffer.remove(buffer.size()-1);
    if (x != minPoint){
      buffer.set(min,x);
      siftDown(min);
    }
    return minPoint;
  }
}
