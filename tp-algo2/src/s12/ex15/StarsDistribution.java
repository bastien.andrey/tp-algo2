
package s12.ex15;

public class StarsDistribution {
  // Rules:
  // - every cell gets at least one star
  // - when t[i] == t[i+1], both cells get the same number of stars
  // - when t[i] >  t[i+1], cell i gets more  stars than cell i+1
  // - when t[i] <  t[i+1], cell i gets fewer stars than cell i+1
  // Returns the total number of stars needed, according to the given rules. 
  // O(n) in CPU, but strict O(1) in RAM
  public static int initStars(float[] t)
  {
    int numOfStars = 1;
    for (int i = 0; i < t.length - 1; i++)
    {
      if (t[i] > t[i+1]) numOfStars++;
      else if (t[i] < t[i+1]) numOfStars--;
    }
    return numOfStars;
  }

  public static int nbOfStars(float[] t) {
    int sumOfStars = 0;
    int numOfStars;
    int previousNumOfStars = 1;
    int occ = 0;

    for (int i = 0; i < t.length; i++)
    {
      numOfStars = previousNumOfStars;

      if (i > 0 && t[i-1] == t[i]) occ++;
      else occ = 0;

      if (i < t.length - 1 && t[i] > t[i+1])
      {
        numOfStars++;
        if (occ > 0 && t[i] != t[i+1])
        {
          sumOfStars += occ;
        }
      }
      else if (i < t.length - 1 && t[i] < t[i-1])
      {
        numOfStars--;
      }
      sumOfStars += numOfStars;
      previousNumOfStars = numOfStars;
    }

    return sumOfStars;
  }
  
  static void demo() {
    // 2 1 1 3 3 2 1 2 = 15
    float[] t = {4,2,2,8,8,3,1,5};
    System.out.println(nbOfStars(t));  // 15
  }
  
  public static void main(String[] args) {
    demo();
  }
}
