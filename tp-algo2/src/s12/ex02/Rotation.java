
package s12.ex02;

import java.util.Arrays;

public class Rotation {
  // must be O(n) in CPU, and O(1) in RAM
  public static void rotate(int[] t, int i) {
    if (t.length < 1) throw new IllegalArgumentException("t should have at least two elements to allow a rotation to operate");
    int nbEltsMoved = 0;
    int index = t.length -1;
    int lastOverRiddenElt = t[index]; // setting a default value, t[index] is not really overridden yet to be clear
    if (t.length%2 != 0) {
      while (nbEltsMoved < t.length){
        int newIndex = (((index-i) % t.length )+ t.length) % t.length;
        int toBeOverridden = t[newIndex];
        t[newIndex] = lastOverRiddenElt;
        lastOverRiddenElt = toBeOverridden;
        nbEltsMoved++;
        index = newIndex;
      }
    }
    else{
      int newDecrement = t.length - i;
      for (int j = t.length-1; j > 1; j--) {
        int newIndex = j-newDecrement >= 0 ? j-newDecrement : j-i;
        int toBeSwapped = t[newIndex];
        t[newIndex] = t[j];
        t[j] = toBeSwapped;
      }
    }
  }
  
  public static void main(String[] args) {
    int[] t = {0,1,2,3,4};
    rotate(t, 2); 
    System.out.println(Arrays.toString(t)); //  --> [2, 3, 4, 0, 1]
    t = new int[]{0,1,2,3};
    rotate(t, 2); 
    System.out.println(Arrays.toString(t)); //  --> [2, 3, 0, 1]
    t = new int[]{0,1,2,3,4,5};
    rotate(t, 2);
    System.out.println(Arrays.toString(t)); //  --> [2, 3, 4, 5, 0, 1]
    t = new int[]{0,1,2,3,4,5};
    rotate(t, 4);
    System.out.println(Arrays.toString(t)); //  --> [4, 5, 0, 1, 2, 3]
  }
}
