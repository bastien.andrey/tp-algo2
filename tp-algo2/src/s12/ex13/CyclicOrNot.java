
package s12.ex13;

import java.util.Random;

public class CyclicOrNot {
  public static class LNode { 
    LNode next;
    LNode(LNode n) { next=n; }
  }
  // ===============================
  
  // O(n) in CPU, but strict O(1) in RAM
  public static boolean hasLoop(LNode head) {
    LNode last = reverseNodes(head);
    // if the last reversed node is head
    // there is a cycle
    boolean loop = last == head;
    // reverse all nodes to make the link like before
    reverseNodes(last);
    return loop;
  }

  public static LNode reverseNodes(LNode head) {
    LNode current = head;
    LNode previous = null;

    // Travel all nodes and reverse nodes link
    while (current.next != null) {
      LNode next = current.next;
      current.next = previous;
      previous = current;
      current = next;
    }

    // last iteration
    current.next = previous;

    return current;
  }
  
  static LNode makeList(int n, boolean hasLoop, Random rnd) {
    LNode last = new LNode(null);
    LNode first = last;
    for(int i=0; i<n; i++) 
      first = new LNode(first);
    if (hasLoop) {
      int x = rnd.nextInt(n);
      LNode anchor = first;
      for(int i=0; i<x; i++)
        anchor = anchor.next;
      last.next = anchor;
    }
    return first;
  }
  
  public static void demo() {
    Random rnd = new Random();
    LNode a = makeList(20, false, rnd);
    LNode b = makeList(20, true,  rnd);
    LNode c = makeList(30, false, rnd);
    LNode d = makeList(30, true,  rnd);
    LNode e = makeList(40, false, rnd);
    LNode f = makeList(40, true,  rnd);
    System.out.println(hasLoop(a));
    System.out.println(hasLoop(a));
    System.out.println(hasLoop(b));
    System.out.println(hasLoop(b));
    System.out.println(hasLoop(c));
    System.out.println(hasLoop(d));
    System.out.println(hasLoop(e));
    System.out.println(hasLoop(f));
  }

  public static void main(String[] args) {
    demo();
  }
  
}
