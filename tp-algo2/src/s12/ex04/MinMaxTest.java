package s12.ex04;

import org.junit.Test;
import static org.junit.Assert.*;
public class MinMaxTest {
    @Test
    public void simpleTest(){
        //best naive case
        long[] t = new long[] {0,1,2,3,4,5,6,7,8};
        assertEquals("minMax not correct",FindMindAndMax.minAndMax(t), new FindMindAndMax.MinAndMax(0,8));
        //worst naive case
        long[] t2 = new long[] {8,7,6,5,4,3,2,1,0};
        assertEquals("minMax not correct",FindMindAndMax.minAndMax(t2), new FindMindAndMax.MinAndMax(0,8));

        //best case pair-reviewing (n is odd)
        long[] t3 = new long[] {0,1,2,3,4,5,6,7,8};
        assertEquals("minMax not correct",FindMindAndMax.minAndMax(t3), new FindMindAndMax.MinAndMax(0,8));
        //worst case pair-reviewing (n is even)
        long[] t4 = new long[] {0,1,2,3,4,5,6,7};
        assertEquals("minMax not correct",FindMindAndMax.minAndMax(t4), new FindMindAndMax.MinAndMax(0,7));
    }
}
