
package s12.ex04;

import org.apache.commons.math3.analysis.function.Min;

public class FindMindAndMax {
  public record MinAndMax(long min, long max) {}
  //======================================
  // Minimize the number of comparisons between elements (i.e. calls to
  // compareTo() if we adapt for an array of Comparable) :
  // - worst-case: no more than 1.7n comparisons
  // - best-case:  no more than 1.1n comparisons 
  // Note that finding the min needs (n-1) comparisons, thus the naive 
  // algorithm involves 2(n-1) comparisons)

  /***
   * @author NoeJuza
   * @param t a non-empty table that you want to find the min and max of
   * @return a MinAndMax record that holds the min value and max value in the array
   */
  public static MinAndMax minAndMax(long[] t) {
    int nbCompairisons = 0;
    assert t.length > 0;
    long min = t[0],max = t[0];
    if (t.length == 1) return new MinAndMax(min,max);
    //naive algorithm worst case 2n-ish:
    //for (long l : t) {
    //  if (l > max) {
    //    max = l;
    //  } else if (l < min) {
    //    min = l;
    //  }
    //}
    //Better idea; comparing by pairs, skipping "half" of the iterations
    for (int i = 1; i < t.length -1; i+= 2) {
      if (t[i] > t[i+1]){
        if (t[i] > max) max = t[i];
        if (t[i + 1] < min) min = t[i+1];
      }else {
        if (t[i] < min) min = t[i];
        if (t[i+1] > max) max = t[i+1];
      }
    }
    // in the case length is even, we need to check one last time with the last element (wasn't checked in the loop
    // so it's fine complexity-wise)
    if (t.length % 2 == 0){
      if (t[t.length-1] > max) max = t[t.length-1];
      if (t[t.length-1] < min) min = t[t.length-1];
    }
    return new MinAndMax(min,max);
  }
}
