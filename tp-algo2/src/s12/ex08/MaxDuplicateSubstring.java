
package s12.ex08;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MaxDuplicateSubstring {
  // Design an approach significantly better than the naive algorithm below
  // (and try to discuss the respective worst-case O(…) complexities)
  /***
   * Slightly better design of the maxDuplicate method,
   * using a HashMap to store how many times a given substring is repeated in the String
   * @param s the string to be checked
   * @return "" if the string has no repeating substring at least two times or the longest repeated string if it has one
   */
  public static String maxDuplicate(String s) {
    HashMap<String,Integer> substrOccurences = new HashMap<>();
    for (int i = 0; i < s.length(); i++) {
      for (int j = s.length() - 1; j >= i; j--) {
        String substr = s.substring(i, j + 1);
        substrOccurences.put(substr, substrOccurences.get(substr) != null ? substrOccurences.get(substr)  + 1 : 1);
      }
    }
    HashMap.Entry<String,Integer> max = Map.entry("",-1);
    for (HashMap.Entry<String,Integer> entry : substrOccurences.entrySet()) {
      if (max.getKey().length() < entry.getKey().length()
              && entry.getValue() > 1) max = entry;
    }
    return max.getKey();
  }
  
  public static String maxDuplicateNaive(String s) {
    String res = "";
    for(int i=0; i<s.length(); i++) {
      for(int j=i+1; j<s.length(); j++) {
        for(int len=res.length()+1; len<=s.length()-j; len++) {
          String a = s.substring(i, i+len);
          String b = s.substring(j, j+len);
          if(!a.equals(b)) break;
          res = a;
        }
      }
    }
    return res;
  }
  
  public static void main(String[] args) {
    String s = "abc defc defc defgh i";
    System.out.println("maxDuplicateNaive(\"abc defc defc defgh i\"): \"" +maxDuplicateNaive(s)+ "\"");
    System.out.println("maxDuplicate(\"abc defc defc defgh i\"): \"" +maxDuplicate(s)+ "\"");
    String s2 = "abcdefghijk";
    System.out.println("maxDuplicateNaive(\"abcdefghijk\"): \"" + maxDuplicateNaive(s2)+ "\"");
    System.out.println("maxDuplicate(\"abcdefghijk\"): \"" +maxDuplicate(s2)+ "\"");
  }
}
