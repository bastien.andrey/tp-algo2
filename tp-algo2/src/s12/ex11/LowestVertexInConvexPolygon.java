
package s12.ex11;

import java.awt.Point;

public class LowestVertexInConvexPolygon {
  // PRE: polygon is convex polygon
  // POST: polygon[result] has minimal Y coordinate
  // Worst-case O(ln n) CPU
  public static int lowest(Point[] polygon) {
    // On prend le premier point du polygon
    int idx = 0;
    int interval = polygon.length;
    Point p = polygon[0];

    // On regarde les éléments gauche et droite du point
    // Et on prend le plus petit
    while (interval > 1) {
      int i = idx - 1 < 0 ? polygon.length - 1 : idx - 1;
      Point left = polygon[i];
      Point right = polygon[idx + 1 % polygon.length];
      interval /= 2;
      if (left.y < p.y) {
        p = left;
        idx -= interval;
        if (idx < 0) idx = polygon.length - 1 + idx;
      } else {
        p = right;
        idx += interval;
      }
    }

    return p.y;
  }
}
