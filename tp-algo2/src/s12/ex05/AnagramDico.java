package s12.ex05;
import java.util.*;

// RAM O(n)
public class AnagramDico {
  HashMap<char[], Set<String>> dico = new HashMap<>();
  // worst-case CPU O(n ln n)
  /***
   * Constructor, defines a new AnagramDictionary Using a Hashmap<char[], Set<String>>
   * @author NoeJuza
   * @param words all words in the dictionary
   */
  public AnagramDico(String[] words) {
    for (String word : words){
      char[] chars = word.toCharArray();
      Arrays.sort(chars);
      if (dico.containsKey(chars)){
        dico.get(chars).add(word);
      }else{
        Set<String> set = new HashSet<String>();
        set.add(word);
        dico.put(chars,set);
      }
    }
  }
  
  // worst-case CPU O(K + ln n);  K=res.size(), output-sensitive

  /**
   * Used to get all the anagrams of a given word
   * @author NoeJuza
   * @param s word to find the anagrams of
   * @return all the anagrams present in the dictionary
   */
  public Set<String> anagramsOf(String s) {
    char[] chars = s.toCharArray();
    Arrays.sort(chars);
    return dico.get(chars);
  }

  // ----------------------------------------------------------------------
  static void demo() {
    String[] t = {"spot","zut","pots", "stop", "hello"};
    AnagramDico d = new AnagramDico(t);
    d = new AnagramDico(t);
    var res = d.anagramsOf("tops");
    System.out.println(res); // [stop, spot, pots]
  }
  
  public static void main(String[] args) {
    demo();
  }
}
