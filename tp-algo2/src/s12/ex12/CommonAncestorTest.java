package s12.ex12;
import org.junit.Test;
import s12.ex04.FindMindAndMax;

import static org.junit.Assert.*;
public class CommonAncestorTest {
    @Test
    public void simpleTest(){

        CommonAncestor.BTNode n1 = new CommonAncestor.BTNode("a",null,null,null);
        CommonAncestor.BTNode n2 = new CommonAncestor.BTNode("b", null, null, n1);
        CommonAncestor.BTNode n3 = new CommonAncestor.BTNode("b", null, null, n1);
        n1.left = n2;
        n1.right = n3;
        CommonAncestor.BTNode n4 = new CommonAncestor.BTNode("b", null, null, n2);
        CommonAncestor.BTNode n5 = new CommonAncestor.BTNode("b", null, null, n2);
        n2.left = n4;
        n2.right = n5;
        CommonAncestor.BTNode n6 = new CommonAncestor.BTNode("b", null, null, n4);
        n4.left = n6;
        assertEquals("something is wrong with commonAncestor", CommonAncestor.commonAncestor(n2,n3),n1);
        assertEquals("something is wrong with commonAncestor", CommonAncestor.commonAncestor(n4,n5),n2);
        assertEquals("something is wrong with commonAncestor", CommonAncestor.commonAncestor(n3,n5),n1);
        assertEquals("something is wrong with commonAncestor", CommonAncestor.commonAncestor(n4,n6),n2);
    }
}
