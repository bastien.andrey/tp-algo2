
package s12.ex12;

public class CommonAncestor {
  static class BTNode {
    Object elt;
    BTNode left, right, parent;

    public BTNode(Object e, BTNode l, BTNode r, BTNode p) {
      elt = e; left = l; right = r; parent = p;
    }
  }
  //===============================================
  // O(height) in CPU, but strict O(1) in RAM

  /***
   * @author NoeJuza
   * @param a first node
   * @param b second node
   * @return the first common ancestor of a and b in the tree, if a and b are null, returns null
   */
  public static BTNode commonAncestor(BTNode a, BTNode b) {
    if (a.parent == null || b.parent == null) return null;
    BTNode internalA, internalB;
    if (a.parent == b.parent) return a.parent;
    int depthA = depth(a);
    int depthB = depth(b);
    internalA = a;
    internalB = b;
    // placing internala or internalb on the highest level between (a,b)
    while (depthA != depthB){
      if (depthA > depthB){
        internalA = internalA.parent;
        depthA--;
      }else{
        internalB = internalB.parent;
        depthB--;
      }
    }
    while (internalA.parent != null) {
      if (internalA.parent == internalB.parent) return internalA.parent;
      internalA = internalA.parent;
      internalB = internalB.parent;
    }
    return internalA; // in this section internalA is always root (matter of fact internalB is too)
  }

  /***
   * @author NoeJuza
   * @param a the node you want to know the depth of
   * @return the depth at which a is located
   */
  private static int depth(BTNode a){
    BTNode parent = a.parent;
    int d = 0;
    while (parent != null){
      parent = parent.parent;
      d++;
    }
    return d;
  }
}
