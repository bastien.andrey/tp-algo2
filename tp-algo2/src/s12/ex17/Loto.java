
package s12.ex17;

import java.util.BitSet;
import java.util.Random;

public class Loto {
  private final Random rnd;
  private int numPulls;
  private long[] elts;
  // PRE: no duplicates, length > 0
  public Loto(long[] elements, Random r) {
    this.rnd = r;
    this.elts = elements;
    this.numPulls =0;
  }

  // Worst-case O(1) CPU
  /**
   * @author NoeJuza
   * @return the pulled value
   * Pulls a new value "loto-style"
   * If #i counts "how often element n° i has been chosen",
   * for any i,j we must ensure that |#i - #j| <= 1
   */
  public long next() {
    if (numPulls < elts.length-1){
      int pulledIndex = numPulls + rnd.nextInt(elts.length-1 - numPulls);
      long pulledValue = elts[pulledIndex];
      elts[pulledIndex] = elts[numPulls];
      elts[numPulls] = pulledValue;
      numPulls++;
      return pulledValue;
    }
    numPulls = 0;
    return elts[elts.length-1];
  }
}
