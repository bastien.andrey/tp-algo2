package s12.ex17;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import static org.junit.Assert.*;

public class LotoTest {
    @Test
    public void testSimple() {
        Map<Long,Integer> map = new HashMap<Long,Integer>();
        long[] elts = new long[] {10,15,20,31,45,51,59};
        Random r = new Random();
        Loto myLoto = new Loto(elts,r);
        for (int i = 0; i < 7; i++) {
            long pulled = myLoto.next();
            assertEquals(pulled + " was pulled more than once", map.get(pulled),null);
            map.put(pulled,1);
        }
        map.forEach((Long key, Integer value) ->{
            System.out.println(key + ":" + value);
        });
        for (int i = 0; i < 7; i++) {
            long pulled = myLoto.next();
            map.put(pulled,map.get(pulled)+1);
        }
        map.forEach((Long key, Integer value) ->{
            assertEquals(key.toString() + " was not pulled enough times",2,(long)value);
            System.out.println(key + ":" + value);
        });
    }
}
