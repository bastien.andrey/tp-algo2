
package s12.ex10;

import java.awt.Point;

public class FairPolygonSplitting {
  public record SplitSolution(int from, int to, double cost) {
    // one part is made of corners from, from+1, ..., to
    // cost is abs(perimA - perimB), we want to minimize that
    
    @Override public String toString() {
      return String.format("[%d,%d : %f]", from, to, cost);
    }
  }
  //=============================================
  // Worst-case CPU O(n)
  public static SplitSolution fairSplit(Point[] polygon) {
    // Calcul du périmètre total et des distances entre chaque point
    double totalPerimeter = 0;
    double[] distances = new double[polygon.length];
    for (int i = 0; i < polygon.length; i++) {
      double distance = distance(polygon[i], polygon[(i + 1) % polygon.length]);
      distances[i] = distance;
      totalPerimeter += distance;
    }

    // Parcourir le tableau des distances pour calculer le périmètre optimal
    double optimalPerimeter = totalPerimeter / 2;
    SplitSolution[] optimalPerimeters = new SplitSolution[polygon.length];
    for (int i = 0; i < polygon.length; i++) {
      double currentPerimeter = 0;
      int j = i;
      while (currentPerimeter < totalPerimeter) {
        currentPerimeter += distances[j];
        j = (j + 1) % polygon.length;
      }
      // Determiné le périmètre optimal
      double previousDistance = j - 1 < 0 ? distances[distances.length - 1] : distances[j-1];
      if (optimalPerimeter - (currentPerimeter - previousDistance) < currentPerimeter - optimalPerimeter) {
        optimalPerimeters[i] = new SplitSolution(i, j-1 < 0 ? distances.length - 1 : j - 1, currentPerimeter - previousDistance);
      }
      else {
        optimalPerimeters[i] = new SplitSolution(i, j, currentPerimeter);
      }
    }

    SplitSolution bestPerimeter = optimalPerimeters[0];

    for (SplitSolution solution : optimalPerimeters) {
      if (optimalPerimeter - solution.cost < optimalPerimeter - bestPerimeter.cost) {
        bestPerimeter = solution;
      }
    }

    return bestPerimeter;
  }

  public static double distance(Point p1, Point p2) {
    return Math.sqrt(Math.pow(Math.abs(p1.x - p2.x), 2) + Math.pow(Math.abs(p1.y - p2.y), 2));
  }

  public static void main(String[] args) {
    Point[] polygon = new Point[] {new Point(5, 3), new Point(4, 1), new Point(3, 0),
            new Point(2, 2), new Point(2, 4)};

    System.out.println(fairSplit(polygon));
  }
}
