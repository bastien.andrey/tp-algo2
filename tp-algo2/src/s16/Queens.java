package s16;

import s16.QueensGui;

public class Queens {
  public interface QueenBoard {
    int     boardSize();
    /** true if that (free or occupied) cell is already threatened */
    boolean isSquareAttacked(int row, int col);
    void    putQueen        (int row, int col);
    void    removeQueen     (int row, int col);
    boolean isQueenThere    (int row, int col);
  }
  //=============================================================

  public static boolean isSolvable(QueenBoard b) {
    // b is an 'inout' parameter
    return isSolvableFromColumn(0, b);
  }

  // The job is to place a queen in each of the columns col, col+1, col+2...
  // so that no queen is threatened by another. 
  // If a solution is found, returns true and gives the solution
  // in board. Otherwise, returns false and keeps board as it was received.

  /**
   * @author NoeJuza
   * @param col the column we want to check
   * @param board the board containing its current state
   * @return true if the board has a solution (the solution is stored in the board) for the given column in the current state, if no solution is found, return false
   */
  public static boolean isSolvableFromColumn(int col,
                         /* inout */  QueenBoard board) {
    if (col >= board.boardSize()) return true; // if we reach the end of the board column-wise we have a solution
    return isSolvableFromCell(0,col,board);
  }
  /**
   * @author NoeJuza
   * @param row the index of the row you want to check from
   * @param col the index of the column you want to check from
   * @param board the board containing its current state
   * @return true if there is a solution (the solution is in the board) for the current state of the game from the provided cell
   */
  public static boolean isSolvableFromCell(int row ,int col, QueenBoard board){
    if (row >= board.boardSize()) return false; // reached the bottom of the board without finding a solution
    if (!board.isSquareAttacked(row,col)){
      board.putQueen(row,col);
      if (!isSolvableFromColumn(col+1,board)){ // do the work on the next column and check if there's a solution
        board.removeQueen(row,col);
      }else{
        return true;
      }
    }
    return isSolvableFromCell(row+1,col,board); // means the current cell was under attacked and we want to check the one below it
  }
  //=============================================================
  static class QueenBoardBasic implements QueenBoard {
    private final boolean[][] hasQueen;
    private final int[] inRow;
    private final int[] inCol;
    private final int[] inDiagonal1;
    private final int[] inDiagonal2;
    private final int size;

    public QueenBoardBasic(int dim) {
      size = dim;
      hasQueen = new boolean[size][size];
      inRow = new int[size];
      inCol = new int[size];
      inDiagonal1 = new int[2 * size - 1];
      inDiagonal2 = new int[2 * size - 1];
    }

    public int boardSize() {
      return size;
    }

    public void putQueen(int row, int col) {
      if (hasQueen[row][col]) throw new IllegalArgumentException("one queen is already there...");
      hasQueen[row][col] = true;
      inRow[row]++;
      inCol[col]++;
      inDiagonal1[row + col]++;
      inDiagonal2[row - col + size - 1]++;
    }

    public void removeQueen(int row, int col) {
      if (!hasQueen[row][col]) throw new IllegalArgumentException("no queen is there...");
      hasQueen[row][col] = false;
      inRow[row]--;
      inCol[col]--;
      inDiagonal1[row + col]--;
      inDiagonal2[row - col + size - 1]--;
    }

    public boolean isSquareAttacked(int row, int col) {
      int neededCount = isQueenThere(row, col) ? 1 : 0;
      return inRow[row] > neededCount
          || inCol[col] > neededCount
          || inDiagonal1[row + col] > neededCount
          || inDiagonal2[row - col + size - 1] > neededCount;
    }

    public String toString() {
      String res = "";
      for (int i = 0; i < size; i++, res += "\n")
        for (int j = 0; j < size; j++)
          res += (hasQueen[i][j]) ? "♛ " : "- ";
      return res;
    }

    public boolean isQueenThere(int row, int col) {
      return hasQueen[row][col];
    }
  }

  //=============================================================

  private static void checkSolution(QueenBoard b) {
    int n = b.boardSize();
    int queensCount = 0;
    for(int i=0; i<n; i++) {
      for(int j=0; j<n; j++) {
        if(!b.isQueenThere(i, j)) continue;
        queensCount++;
        if(b.isSquareAttacked(i, j)) throw new IllegalStateException("attacked queen!");
      }
    }
    if(queensCount != n) 
      throw new IllegalStateException("bad number of queens!");
  }

  public static void solve(int n) {
    QueenBoard b = new QueenBoardBasic(n);
    if (isSolvable(b)) {
      System.out.println("Found !\n" + b);
      checkSolution(b);
    } else {
      System.out.println("Not found !");
    }
  }

  public static void main(String[] args) {
    boolean WITH_GUI=true;      // TODO: toggle when measuring performances ;)
    int size = 8;
    if (args.length != 0) size = Integer.parseInt(args[0]);
    if (WITH_GUI) {
      int slowdownMs = 100;
      //QueensGui.main(new String[]{""+size, ""+slowdownMs});
    } else {
      solve(size);
    }
  }

}
