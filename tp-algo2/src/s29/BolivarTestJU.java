
package s29;

import java.security.KeyPair;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class BolivarTestJU {
  KeyPair john, kurt, mary;
  String johnAccount, kurtAccount, maryAccount;
  
  @Before
  public void prepareThreeAccounts() {
    john = CryptoUtils.newRsaKeyPair();
    kurt = CryptoUtils.newRsaKeyPair();
    mary = CryptoUtils.newRsaKeyPair();
    johnAccount = CryptoUtils.stringFromPublicKey(john.getPublic());
    kurtAccount = CryptoUtils.stringFromPublicKey(kurt.getPublic());
    maryAccount = CryptoUtils.stringFromPublicKey(mary.getPublic());
  }

  @Test(expected=BadTransactionException.class)
  public void signedByWrongPerson() throws BadTransactionException, BadBlockchainException {
    BolivarBlockchain bbc = new BolivarBlockchain(3, 1);
    MoneyTransfer johnToMary = new MoneyTransfer(1, johnAccount, maryAccount);
    Transaction tr0 = Transaction.signedTransaction(johnToMary, 0, mary.getPrivate());
    bbc.handleNewTransaction(tr0);
    System.out.println("oops " + bbc.bank());
  }

  @Test
  public void aCoupleOfValidTransactions() throws BadTransactionException, BadBlockchainException {
    BolivarBlockchain bbc = new BolivarBlockchain(3, 1);
    MoneyTransfer johnToKurt  = new MoneyTransfer(7, johnAccount, kurtAccount);
    MoneyTransfer johnToMary1 = new MoneyTransfer(8, johnAccount, maryAccount);
    MoneyTransfer maryToKurt  = new MoneyTransfer(3, maryAccount, kurtAccount);
    MoneyTransfer johnToMary2 = new MoneyTransfer(6, johnAccount, maryAccount);
    MoneyTransfer kurtToMary  = new MoneyTransfer(5, kurtAccount, maryAccount);
    MoneyTransfer johnToMary3 = new MoneyTransfer(4, johnAccount, maryAccount);
    List<Transaction> tl = List.of(
        Transaction.signedTransaction(johnToKurt,  0, john.getPrivate()),
        Transaction.signedTransaction(johnToMary1, 1, john.getPrivate()),
        Transaction.signedTransaction(maryToKurt,  0, mary.getPrivate()),
        Transaction.signedTransaction(johnToMary2, 2, john.getPrivate()),
        Transaction.signedTransaction(kurtToMary,  0, kurt.getPrivate()),
        Transaction.signedTransaction(johnToMary3, 3, john.getPrivate())
      );
    for(Transaction t: tl) {
      bbc.handleNewTransaction(t);
    }
    BolivarBank bank = bbc.bank();
    assertEquals( 75, bank.moneyOf(johnAccount));
    assertEquals(105, bank.moneyOf(kurtAccount));
    assertEquals(120, bank.moneyOf(maryAccount));
  }

}
