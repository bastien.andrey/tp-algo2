package s03;

/** For this priority queue, we consider that a "small" value
 *  of type P represents a stronger priority than a "big" one  */
public class PtyQueue<E, P extends Comparable<P>> {
  private final Heap<HeapElt> heap;
  private long creationIdx = 0;

  public PtyQueue() {
    heap = new Heap<>();
  }

  public boolean isEmpty() {
    return heap.isEmpty();
  }

  /** Adds an element elt with priority pty */ 
  public void enqueue(E elt, P pty) {
    heap.add(new HeapElt(pty, elt, creationIdx));
    creationIdx++;
  }
  
  /** Returns the element with strongest priority. PRE: !isEmpty() */ 
  public E consult() {
    return heap.min().ele;
  }

  /** Returns the priority of the element with strongest priority. 
   *  PRE: !isEmpty() */ 
  public P consultPty() {
    return heap.min().prt;
  }
  
  /** Removes and returns the element with strongest priority. 
   *  PRE: !isEmpty() */ 
  public E dequeue() {
    E min = heap.min().ele;
    heap.removeMin();
    return min;
  }

  @Override public String toString() {
    return heap.toString(); 
  }
  //=============================================================
  class HeapElt implements Comparable<HeapElt> {
    E ele;
    P prt;
    long creationIndex;
    
    public HeapElt(P thePty, E theElt, long creationIndex) {
      this.ele = theElt;
      this.prt = thePty;
      this.creationIndex = creationIndex;
    }

    @Override public int compareTo(HeapElt arg0) {
      if (prt.compareTo(arg0.prt) == 0) {
        if (creationIndex < arg0.creationIndex) return -1;
        else return 1;
      }
      else {
        return prt.compareTo(arg0.prt);
      }
    }
  }
}

