package s18;
import java.util.*;

public class Dijkstra {
  // ================================================
  record Vertex(int vid, long pty) implements Comparable<Vertex> {
    @Override public int compareTo(Vertex v) {
      return Long.compare(this.pty,  v.pty);
    }
  }
  // ================================================

  // POST : minDist[i] is the min distance from a to i
  //                      MAX_VALUE if i is not reachable, 0 for a
  //         parent[i] is the parent of i in the corresponding tree
  //                      -1 if i is not reachable, and for vertex a
  public static void dijkstra(WeightedDiGraph g, int a, 
                              long[] minDist, int[] parent) {
    int size = g.nbOfVertices();
    boolean[] isVisited = new boolean[size];
    for (int i = 0; i < size; i++) {
      minDist[i] = Long.MAX_VALUE;
      isVisited[i] = false;
      parent[i] = -1;
    }
    PriorityQueue<Vertex> pq = new PriorityQueue<>();
    pq.add(new Vertex(a, minDist[a]));
    minDist[a] = 0;
    while (!pq.isEmpty()) {
      Vertex v = pq.remove();
      if (isVisited[v.vid]) continue;
      isVisited[v.vid] = true;
      for (int n : g.neighboursFrom(v.vid)) {
        long dist = minDist[v.vid] + g.edgeWeight(v.vid, n);
        if (dist < minDist[n]) {
          minDist[n] = dist;
          pq.add(new Vertex(n, minDist[n]));
          parent[n] = v.vid;
        }
      }
    }
  }

  /** returns all vertices in vid's strongly connected component */
  static Set<Integer> strongComponentOf(WeightedDiGraph g, int vid) {
    Set<Integer> res = new HashSet<>();
    res.add(vid);
    WeightedDiGraph reversedGraph = new WeightedDiGraph(g.nbOfVertices());

    for (int i = 0; i < g.nbOfVertices(); i++) {
      for (int neighbour : g.neighboursFrom(i)) {
        reversedGraph.putEdge(neighbour, i, g.edgeWeight(i, neighbour));
      }
    }

    long[] minDist = new long[g.nbOfVertices()];
    int[] parent = new int[g.nbOfVertices()];
    dijkstra(g, vid, minDist, parent);

    long[] minDistReversed = new long[g.nbOfVertices()];
    int[] parentReversed = new int[g.nbOfVertices()];
    dijkstra(reversedGraph, vid, minDistReversed, parentReversed);

    for (int i = 0; i < g.nbOfVertices(); i++) {
      if (parent[i] >= 0 && parentReversed[i] >= 0) {
        res.add(i);
      }
    }

    return res;
  }

  // ------------------------------------------------------------
  public static void main(String [] args) {
    int nVertices = 6; // int nEdges = 12;
    final int A=0, B=1, C=2, D=3, E=4, F=5;
    int[] srcs  = {A, A, A, B, B, D, D, D, D, E, F, F };
    int[] dsts  = {B, C, F, F, C, A, B, C, E, A, D, E };
    int[] costs = {12,6, 14,1, 7, 9, 3, 2, 4, 5, 10,11};

    WeightedDiGraph g = new WeightedDiGraph(nVertices, srcs, dsts, costs);
    System.out.println("Input Graph: " + g);

    int n = g.nbOfVertices();
    long[] minCost = new long[n];
    int [] parent  = new int [n];
    for (int a=0; a<n; a++) {
      dijkstra(g, a, minCost, parent);
      System.out.println("\nMinimal distances from " + a); 
      for (int i=0; i<minCost.length; i++) {
        String s = "to "+ i +":";
        if (minCost[i] == Long.MAX_VALUE) s += " unreachable";
        else s += " total " + minCost[i] + ", parent " + parent[i];
        System.out.println(s);
      }
    }
  }
}
