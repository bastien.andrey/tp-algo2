package tsp;

//======================================================================
// Optimization: Extract Double.MAX_VALUE in a constant
public class TSP17 implements TSP {
    public void salesman(TSPPoint [] t, int [] path) {
        int i, j;
        int n = t.length;
        boolean[] visited = new boolean[n];
        int thisPt, closestPt = 0;
        double shortestDist;

        thisPt = n-1;
        if (thisPt < 0) return;
        visited[thisPt] = true;
        path[0] = n-1;  // chose the starting city
        double[] tvx = new double[t.length];
        double[] tvy = new double[t.length];

        for (int k = 0; k < t.length; k++) {
            tvx[k] = t[k].x;
            tvy[k] = t[k].y;
        }

        for(i=1; i<n; i++) {
            shortestDist = Double.MAX_VALUE;
            double tpty = tvy[thisPt];
            double tptx = tvx[thisPt];

            for(j=0; j<n; j++) {
                if (visited[j])
                    continue;
                double tx = tvx[j];
                if ( ((tptx-tx)*(tptx-tx)) >= shortestDist ) continue;
                double ty = tvy[j];
                if (((tptx-tx)*(tptx-tx)) + ((tpty-ty)*(tpty-ty)) < shortestDist  ) {
                    shortestDist = ((tptx - tx) * (tptx - tx)) + ((tpty - ty) * (tpty - ty));
                    closestPt = j;
                }
            }
            visited[closestPt] = true;
            path[i] = closestPt;
            thisPt = closestPt;
        }
    }
}