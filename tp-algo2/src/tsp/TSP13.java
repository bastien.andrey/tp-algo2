package tsp;
//======================================================================
// Optimization: Get out invariants tptx and tpty
public class TSP13 implements TSP {
    public void salesman(TSPPoint [] t, int [] path) {
        int i, j;
        int n = t.length;
        boolean[] visited = new boolean[n];
        int thisPt, closestPt = 0;
        double shortestDist;

        thisPt = n-1;
        if (thisPt < 0) return;
        visited[thisPt] = true;
        path[0] = n-1;  // chose the starting city
        for(i=1; i<n; i++) {
            shortestDist = Double.MAX_VALUE;
            double tptx = t[thisPt].x;
            double tpty = t[thisPt].y;
            for(j=0; j<n; j++) {
                if (visited[j])
                    continue;

                double tx = t[j].x;
                double ty = t[j].y;
                if (Math.sqrt(((tptx-tx)*(tptx-tx)) + ((tpty-ty)*(tpty-ty))) < shortestDist ) {
                    shortestDist = Math.sqrt(((tptx - tx) * (tptx - tx)) + ((tpty - ty) * (tpty - ty)));
                    closestPt = j;
                }
            }

            path[i] = closestPt;
            visited[closestPt] = true;
            thisPt = closestPt;
        }
    }
}
