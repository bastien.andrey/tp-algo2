//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package tsp;

class TSP200 implements TSP {
    TSP200() {
    }

    public void salesman(TSPPoint[] t0, int[] path) {
        int n = t0.length;
        double[] tx = new double[n];
        double[] ty = new double[n];
        int[] ti = new int[n];
        int closestPt = 0;
        int highPt = n - 1;

        for(int i = 0; i < n; ti[i] = i++) {
            tx[i] = t0[i].x;
            ty[i] = t0[i].y;
        }

        int thisPt = n - 1;

        for(path[0] = n - 1; highPt > 0; thisPt = highPt) {
            double shortestDist = Double.MAX_VALUE;
            double thisX = tx[thisPt];
            double thisY = ty[thisPt];
            int j = 0;

            while(true) {
                double d = (thisX - tx[j]) * (thisX - tx[j]);
                if (!(d > shortestDist)) {
                    d += (thisY - ty[j]) * (thisY - ty[j]);
                    if (!(d > shortestDist)) {
                        if (j == highPt) {
                            path[n - highPt] = ti[closestPt];
                            --highPt;
                            double aux = tx[highPt];
                            tx[highPt] = tx[closestPt];
                            tx[closestPt] = aux;
                            aux = ty[highPt];
                            ty[highPt] = ty[closestPt];
                            ty[closestPt] = aux;
                            int auxi = ti[highPt];
                            ti[highPt] = ti[closestPt];
                            ti[closestPt] = auxi;
                            break;
                        }

                        shortestDist = d;
                        closestPt = j;
                    }
                }

                ++j;
            }
        }

    }
}
